<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
<?php print $head ?>
<title><?php print $head_title ?></title>
<?php print $styles ?>
<link type="text/css" rel="stylesheet" href="<?php echo base_path() . path_to_theme(); ?>/css/jquery-ui-1.8.4.custom.css" media="screen"/>
<?php print $scripts ?>
<!--[if lt IE 7]>
      <?php print phptemplate_get_ie7_styles(); ?>
    <![endif]-->
<!--[if lt IE 8]>
      <?php print phptemplate_get_ie8_styles(); ?>
    <![endif]-->
</head>
<body<?php print phptemplate_body_class($left, $right); ?>>
<div class="wrapper"> 
  <!-- logo --> 
  <strong class="logo"><a href="http://pueblo.org">Pueblo, Colorado.</a></strong> <img src="<?php echo base_path() . path_to_theme(); ?>/images/logo-print.gif" alt="PuebloColorado" class="hidden" /> 
  <!-- main -->
  <div id="main" class="threecol-main">
    <div class="main-bg">
      <div class="events">
	<!-- Display Events -->
        <!-- Main Content Template Elements and Tabs -->
        <?php if ($mission): print '<div id="mission">'. $mission .'</div>'; endif; ?>
        <?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block">'; endif; ?>
        <?php if ($tabs): print '<ul class="tabs primary">'. $tabs .'</ul></div>'; endif; ?>
        <?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
        <!-- Headline and Top Submit Button -->
        <div class="headline">
	        <!-- Everyone should be able to see the "Submit and Event" button now. -->
        	<a class="new-event-button" value="Submit An Event »" href="<?php echo base_path() ?>node/add/event">Submit An Event »</a>
        	<h1>Events</h1>
        </div>
        <!-- END Headline and Top Submit Button -->
        <!-- Top Picks Block -->
		<?php if($top_events): ?>
        <div class="top-events">
          <div class="title">
            <h2>Top Picks</h2>
          </div>
          <div class="three-columns"> <?php print $top_events; ?> </div>
        </div>
        <?php endif; ?>
        <!-- END Top Picks Block -->
        
        <?php if ($show_messages && $messages): print $messages; endif; ?>
        <?php print $help; ?>

        
		<?php print $content; ?>
   	</div> 
<!-- Left Sidebar -->
<div class="sidebar orange-bar"> 
    <!-- calendar block -->
    <?php if($left): ?>
    <div id="block-views-calendar-calendar_block_1" class="clear-block block block-views">
      <div class="content">
        <?php print $left; ?>
      </div>
    </div>
    <?php endif; ?>
    <!-- search form
    <div class="heading">
      <h3>Search All Events</h3>
    </div>
    <form class="search-frm" id="filter-events" method="get" action="<?php print url('node/14'); ?>">
      <fieldset>
        <div class="row">
          <input type="hidden" name="event_action" value="eventsearch" />
          <input type="hidden" name="ipp" value="10" />
          <label class="date" for="start-date">Start Date:</label>
          <input class="text" name="startdate" id="start-date" type="text"<?php if($startdate) print ' value="'.$startdate.'"'; ?> />
        </div>
        <div class="row">
          <label class="date" for="end-date">End Date:</label>
          <input class="text" name="enddate" id="end-date" type="text"<?php if($enddate) print ' value="'.$enddate.'"'; ?> />
        </div>
        <div class="row"> <span class="label">Categories:</span>
          <div class="two-columns">
            <?php
                                  $all_terms = custom_get_terms_by_vocabulary(2);
                            ?>
            <div class="row">
              <div class="col">
                <input class="checkbox" id="all" value="0" type="checkbox"<?php if(is_array($event_category)): if(in_array(0,$event_category)) echo ' checked="checked"'; endif; ?> name="event_category[]"/>
                <label for="all">All</label>
              </div>
            </div>
            <?php
                                if(is_array($all_terms)):
                                  $i = 0;
                                  foreach($all_terms as $key=>$vterm):
                                  if($i % 2 == 0) print '<div class="row">';
                            ?>
            <div class="col">
              <input class="checkbox" id="term-<?php print $vterm->tid ?>" name="event_category[]"<?php if(($event_category) && (in_array($vterm->tid,$event_category))): ?> checked="checked"<?php endif ?> value="<?php print $vterm->tid ?>" type="checkbox" />
              <label for="term-<?php print $vterm->tid ?>"><?php print $vterm->name ?></label>
            </div>
            <?php
                                  $i++;
                                  if(( $i % 2 == 0 || count($all_terms) == $i )) print '</div>';
                                  endforeach;
                                endif;
                            ?>
          </div>
        </div>
        <div class="row"> <span class="label">Locations:</span>
          <div class="two-columns">
            <?php
                                  $all_terms = custom_get_terms_by_vocabulary(3);
                            ?>
            <div class="row">
              <div class="col">
                <input class="checkbox" id="all-2" value="0"<?php if(is_array($event_location)): if(in_array(0,$event_location)) echo ' checked="checked"'; endif; ?> type="checkbox" name="event_location[]"/>
                <label for="all-2">All</label>
              </div>
            </div>
            <?php
                                if(is_array($all_terms)):
                                  $i = 0;
                                  foreach($all_terms as $key=>$vterm):
                                  if(($i % 2 == 0)) print '<div class="row">';
                            ?>
            <div class="col">
              <input class="checkbox" id="term-<?php print $vterm->tid ?>" name="event_location[]"<?php if(($event_location) && (in_array($vterm->tid,$event_location))): ?> checked="checked"<?php endif ?> value="<?php print $vterm->tid ?>" type="checkbox" />
              <label for="term-<?php print $vterm->tid ?>"><?php print $vterm->name ?></label>
            </div>
            <?php
                                  $i++;
                                  if(( $i % 2 == 0 || count($all_terms) == $i )) print '</div>';
                                  endforeach;
                                endif;
                            ?>
          </div>
        </div>
        <div class="row">
          <input class="btn-submit" type="submit" value="Search »" />
        </div>
      </fieldset>
    </form> -->
</div>
<!-- End Left Sidebar -->
    </div>
  </div>
  <?php print $breadcrumb; ?> 
<!-- Page header -->
  <div id="header">
    <div class="bg">
      <?php
                    if(is_array($node->field_header_image)){
                     $image = current($node->field_header_image);
                     $image_path = $image['filepath'];
                     print theme('imagecache', 'header_image', $image_path, $title) ;
                    } else {
                      print '<img src="'.  base_path() . path_to_theme() .'/images/visual2.jpg" alt="image description" class="visual-image" />';
                    }
                ?>
      <?php print $header; ?> </div>
  </div>
  <!-- headline -->
  <div id="headline"> 
    <!-- search form -->
    <?php if ($search_box): ?>
    <div class="search"><?php print $search_box ?></div>
    <?php endif; ?>
    <!-- top menu -->
    <ul class="top-menu">
      <!-- <li><a href="#"><span>My pueblo</span></a></li> -->
      <li><a href="/jobs">Jobs</a></li>
      <li><a href="/search">Search:</a></li>
    </ul>
  </div>
</div>
<!-- footer -->
<div id="footer">
  <div class="wrapper"> <?php print $footer; ?> </div>
</div>
<?php print $closure ?>
</body>
</html>
