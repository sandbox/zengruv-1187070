<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
  <head>
    <?php print $head ?>
    <title><?php print $head_title ?></title>
    <?php print $styles ?>
	<link media="screen" href="<?php echo base_path() . path_to_theme(); ?>/css/jquery-ui-1.8.4.custom.css" rel="stylesheet" type="text/css">
    <?php print $scripts ?>
    <!--[if lt IE 7]>
      <?php print phptemplate_get_ie7_styles(); ?>
    <![endif]-->
    <!--[if lt IE 8]>
      <?php print phptemplate_get_ie8_styles(); ?>
    <![endif]-->
  </head>
  <body<?php print phptemplate_body_class($left, $right); ?>>
	<div class="wrapper">
		<!-- logo -->
		<strong class="logo"><a href="http://pueblo.org">Pueblo, Colorado.</a></strong>

		<img src="<?php echo base_path() . path_to_theme(); ?>/images/logo-print.gif" alt="PuebloColorado" class="hidden" />
			<!-- main -->
			<div id="main" class="threecol-main">
			  <div class="main-bg">
					  <div class="events">
						  <?php if ($mission): print '<div id="mission">'. $mission .'</div>'; endif; ?>
						  <?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block">'; endif; ?>
						  <?php if ($tabs): print '<ul class="tabs primary">'. $tabs .'</ul></div>'; endif; ?>
						  <?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
						  <?php if ($show_messages && $messages): print $messages; endif; ?>
						  <?php print $help; ?>
						  <?php print $content; ?>
					  </div>
					  <?php if(isset($secondary_links)): ?>
					  <div class="sidebar orange-bar">
						  <!-- heading -->
						  <div class="heading">
							  <h3>Events</h3>
						  </div>
						  <?php print theme('links', $secondary_links, array('class' => 'events-categories')) ?>
						  <?php
							  if($user->uid):
						  ?>
						  <a class="btn-submit-event" href="<?php echo base_path() ?>node/add/event">Submit an Event</a>
						  <?php
							  endif;
						  ?>
					  </div>
					  <?php endif; ?>
			  </div>
		</div>
        <?php print $breadcrumb; ?>
		<!-- header -->
		<div id="header">
			<div class="bg">
                <?php
                    if(is_array($node->field_header_image)){
                     $image = current($node->field_header_image);
                     $image_path = $image['filepath'];
                     print theme('imagecache', 'header_image', $image_path, $title) ;
                    } else {
                      print '<img src="'.  base_path() . path_to_theme() .'/images/image11.jpg" alt="image description" class="visual-image" />';
                    }
                ?>
                <?php print $header; ?>
			</div>
		</div>
		<!-- headline -->
		<div id="headline">
			<!-- search form -->
            <?php if ($search_box): ?><div class="search"><?php print $search_box ?></div><?php endif; ?>
			<!-- top menu -->
			<ul class="top-menu">
<!-- <li><a href="#"><span>My pueblo</span></a></li> -->
<li><a href="/jobs">Jobs</a></li>
<li><a href="/search">Search:</a></li>
</ul>
		</div>
	</div>
	<!-- footer -->
	<div id="footer">
		<div class="wrapper">
			<?php print $footer; ?>
		</div>
	</div>
  <?php print $closure ?>
  </body>
</html>
