<?php
    if(is_array($node->field_event_thumb)):
     $image = current($node->field_event_thumb);
     $image_path = $image['filepath'];
?>
    <div class="photo-col">
        <div class="photo">
            <div class="bg1">
                <div class="bg2">
                    <div class="bg3">
                        <a href="<?php print $node_url; ?>"><?php print theme('imagecache', 'today_event_thumb', $image_path, $title) ; ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
    endif;
?>
<div class="text">
    <strong class="title"><a href="<?php print $node_url; ?>"><?php print $title; ?></a></strong>
<?php
    $event_teaser = current($node->field_event_teaser);
?>
    <p><?php print $event_teaser['value'] ?></p>
    <span class="more"><a href="<?php print $node_url; ?>">Event Details</a></span>
</div>
