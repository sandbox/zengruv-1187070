<?php
    if(is_array($node->field_thumb)):
     $image = current($node->field_thumb);
     $image_path = $image['filepath'];
?>
    <div class="photo-col">
        <div class="photo">
            <div class="bg1">
                <div class="bg2">
                    <div class="bg3">
                        <a href="<?php print $node_url; ?>"><?php print theme('imagecache', 'big_activity_thumb', $image_path, $title) ; ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
    endif;
    $field_teaser = current($node->field_teaser);
    $field_maps = current($node->field_maps);
    $field_cams = current($node->field_cams);
    $field_lodging = current($node->field_lodging);
    $field_amenities = current($node->field_amenities);
?>
    <div class="descr-col">
        <div class="holder">
            <h3><?php print $title; ?></h3>
            <?php if($field_teaser): print $field_teaser['value']; ?><?php endif; ?>
            <a href="<?php print $node_url; ?>" class="lnk-view lnk-view-green"><?php if($field_readmore['value']!=''){ print $field_readmore['value']; } else { ?>Learn More<?php } ?></a>
        </div>
        <ul class="tools">
            <?php if(($field_maps)&&($field_maps['value']!='#' && $field_maps['value']!='')): ?><li><a href="<?php print $field_maps['value'] ?>">Maps</a></li><?php endif; ?>
            <?php if(($field_cams)&&($field_maps['value']!='#' && $field_maps['value']!='')): ?><li><a href="<?php print $field_cams['value'] ?>">Cams</a></li><?php endif; ?>
            <?php if(($field_lodging)&&($field_maps['value']!='#' && $field_maps['value']!='')): ?><li><a href="<?php print $field_lodging['value'] ?>">Lodging</a></li><?php endif; ?>
            <?php if(($field_amenities)&&($field_maps['value']!='#' && $field_maps['value']!='')): ?><li><a href="<?php print $field_amenities['value'] ?>">Amenities</a></li><?php endif; ?>
        </ul>
    </div>