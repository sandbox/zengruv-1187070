<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
<?php print $head ?>
<title><?php print $head_title ?></title>
<?php print $styles ?>
<link type="text/css" rel="stylesheet" href="<?php echo base_path() . path_to_theme(); ?>/css/jquery-ui-1.8.4.custom.css" media="screen"/>
<?php print $scripts ?>
<!--[if lt IE 7]>
      <?php print phptemplate_get_ie7_styles(); ?>
    <![endif]-->
<!--[if lt IE 8]>
      <?php print phptemplate_get_ie8_styles(); ?>
    <![endif]-->
</head>
<body<?php print phptemplate_body_class($left, $right); ?>>
<div class="wrapper"> 
  <!-- logo --> 
  <strong class="logo"><a href="http://pueblo.org">Pueblo, Colorado.</a></strong>
 <img src="<?php echo base_path() . path_to_theme(); ?>/images/logo-print.gif" alt="PuebloColorado" class="hidden" /> 
  <!-- main -->
  <div id="main"<?php if($node->type != "2col"): ?> class="threecol-main"<?php endif; ?>>
    <div class="main-bg">
      <?php
			      if ($node->type == "2col") {
                    echo '<div class="two-columns magenta-links">';
					print $content;
			      } elseif($node->type == "2col_w_nav") {
                    echo '<div>';
					print $content;
			      } elseif($node->type == "3_columns") {
                    echo '<div class="right-twocol magenta-links">';
					print $content;
                  } else {
                    echo '<div class="right-twocol">';
                    //if ($title): print '<h2 class="main-title-text'. ($tabs ? ' with-tabs' : '') .'">'. $title .'</h2>'; endif;
					echo '<div class="center-col">';
					echo '<div class="text-wrapper">';
					echo '<div class="holder">'; ?>
      <?php if ($mission): print '<div id="mission">'. $mission . '</div>'; endif; ?>
      <?php if ($title): print '<h1>'. $title .'</h1>'; endif; ?>
      <?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block">'; endif; ?>
      <?php if ($tabs): print '<ul class="tabs primary">'. $tabs .'</ul></div>'; endif; ?>
      <?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
      <?php if ($show_messages && $messages): print $messages; endif; ?>
      <?php print $help; ?> <?php print $content; ?> </div>
    <div class="holder"> <span class="horizontal-divider">&nbsp;</span> </div>
    <?php print $visit_bottom; ?> </div>
</div>
<?php }	if($node->type != "2col_w_nav"){ ?>
<div class="aside1">
 <?php
					print $right;
				    print $two_col_right;
                    print $three_col_right;
            ?>
</div>
<?php
				}
			    echo '</div>';
                if($node->type != "2col"):
			?>
<div class="sidebar magenta-bar"><?php print $left; ?> <?php print $two_col_w_nav_left; ?> <?php print $three_col_left; ?> </div>
<?php
                endif;
            ?>
</div>
</div>
<?php print $breadcrumb; ?> 
<!-- header -->
<div id="header">
  <div class="bg">
    <?php
                    if(is_array($node->field_header_image)){
                     $image = current($node->field_header_image);
                     $image_path = $image['filepath'];
                     print theme('imagecache', 'header_image', $image_path, $title) ;
                    } else {
                      print '<img src="'.  base_path() . path_to_theme() .'/images/visual2.jpg" alt="image description" class="visual-image" />';
                    }
                ?>
    <?php print $header; ?> </div>
</div>
<!-- headline -->
<div id="headline"> 
  <!-- search form -->
  <?php if ($search_box): ?>
  <div class="search"><?php print $search_box ?></div>
  <?php endif; ?>
  <!-- top menu -->
  <ul class="top-menu">

<!-- <li><a href="#"><span>My pueblo</span></a></li> -->

<li><a href="/jobs">Jobs</a></li>

<li><a href="/search">Search:</a></li>

</ul>
</div>
</div>
<!-- footer -->
<div id="footer">
  <div class="wrapper"> <?php print $footer ?> </div>
</div>
<?php print $closure ?>
</body>
</html>
