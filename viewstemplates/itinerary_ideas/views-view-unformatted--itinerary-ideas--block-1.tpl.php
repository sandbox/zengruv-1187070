<?php if (!empty($title)): ?>
	<div class="heading green-heading">
		<h3><?php print $title; ?></h3>
	</div>
<?php endif; ?>
<?php foreach ($rows as $id => $row): ?>
<div class="holder post">
    <?php print $row; ?>
</div>
<?php endforeach; ?>
