  <?php if ($header): ?>
    <div class="heading green-heading">
      <?php print $header; ?>
    </div>
  <?php endif; ?>
  <?php if ($rows): ?>
      <?php print $rows; ?>
  <?php elseif ($empty): ?>
      <?php print $empty; ?>
  <?php endif; ?>
