<?php if (!empty($title)): ?>
	<div class="heading">
		<h2><?php print $title; ?></h2>
	</div>
<?php endif; ?>
<?php foreach ($rows as $id => $row): ?>
	<div class="side-box">
		  <?php print $row; ?>
	</div>
<?php endforeach; ?>
