<div class="col">
  <?php if ($header): ?>
      <h3><?php print $header; ?></h3>
  <?php endif; ?>
  <?php if ($rows): ?>
      <?php print $rows; ?>
  <?php elseif ($empty): ?>
      <?php print $empty; ?>
  <?php endif; ?>
</div>