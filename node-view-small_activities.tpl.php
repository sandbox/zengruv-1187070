<?php
    if(is_array($node->field_thumb)):
     $image = current($node->field_thumb);
     $image_path = $image['filepath'];
?>
    <div class="photo">
        <div class="bg1">
            <div class="bg2">
                <div class="bg3">
                    <a href="<?php print $node_url; ?>"><?php print theme('imagecache', 'small_activity_thumb', $image_path, $title) ; ?></a>
                </div>
            </div>
        </div>
    </div>
<?php
    endif;
?>
    <h4><?php print $title; ?></h4>
<?php
	$field_readmore = current($node->field_readmore);
    $field_teaser = current($node->field_teaser);
?>
    <?php if($field_teaser): ?><p><?php print $field_teaser['value']; ?></p><?php endif; ?>
    <p><a href="<?php print $node_url; ?>" class="lnk-view lnk-view-green"><?php print $field_readmore['value'] ?></a></p>