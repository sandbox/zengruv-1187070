<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
<?php print $head ?>
<title><?php print $head_title ?></title>
<?php print $styles ?>
<link type="text/css" rel="stylesheet" href="<?php echo base_path() . path_to_theme(); ?>/css/jquery-ui-1.8.4.custom.css" media="screen"/>
<?php print $scripts ?>
<!--[if lt IE 7]>
      <?php print phptemplate_get_ie7_styles(); ?>
    <![endif]-->
<!--[if lt IE 8]>
      <?php print phptemplate_get_ie8_styles(); ?>
    <![endif]-->
</head>
<body<?php print phptemplate_body_class($left, $right); ?>>
<div class="wrapper"> 
  <!-- logo --> 
  <strong class="logo"><a href="http://pueblo.org">Pueblo, Colorado.</a></strong> <img src="<?php echo base_path() . path_to_theme(); ?>/images/logo-print.gif" alt="PuebloColorado" class="hidden" /> 
  <!-- main -->
  <div id="main" class="threecol-main">
    <div class="main-bg">
      <div class="events">
        <?php
						if(isset($_GET['event_action']) && $_GET['event_action'] == 'eventsearch') {
							$startdate = $_GET['startdate'];
							$enddate = $_GET['enddate'];
							$event_category = $_GET['event_category'];
							$event_location = $_GET['event_location'];
						}
						if(isset($_GET['paged']) && $_GET['paged']>0){
							$current_page = (int)$_GET['paged'];
						} else {
							$current_page = 1;
						}
						if( isset($_GET['ipp']) && $_GET['ipp'] != '') {
						  $items_per_page = (int)$_GET['ipp'];
						} else {
						  $items_per_page = 10;
						}
						$offset = $items_per_page*($current_page-1);
						$pueblo_events = get_pueblo_events($startdate, $enddate, $event_category, $event_location, $items_per_page);
						$events_count = get_pueblo_events_count($startdate, $enddate, $event_category, $event_location);
					?>
        <?php if ($mission): print '<div id="mission">'. $mission .'</div>'; endif; ?>
        <?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block">'; endif; ?>
        <?php if ($tabs): print '<ul class="tabs primary">'. $tabs .'</ul></div>'; endif; ?>
        <?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
        <?php if ($show_messages && $messages): print $messages; endif; ?>
        <?php print $help; ?>
            <div class="headline">
              <a class="new-event-button" value="Submit An Event »" href="<?php echo base_path() ?>node/add/event">Submit An Event »</a>
              <h1>Events</h1>
            </div>
            <?php if($top_events): ?>
            <div class="top-events">
              <div class="title">
                <h2>Top Picks</h2>
              </div>
              <div class="three-columns"> <?php print $top_events; ?> </div>
            </div>
            <?php endif; ?>
            <?php print $content; ?>
            <div class="mapping-events">
              <div class="heading">
                <div class="holder">
                  <div class="frame">
                    <dl class="date">
                      <dt>Showing Events For:</dt>
                      <?php
												  if($startdate):
													  $event_start_date = date('F j, Y',strtotime($startdate));
												?>
                      <dd><?php print $event_start_date; ?></dd>
                      <?php
												  endif;
												  if($enddate):
													  $event_end_date = date('F j, Y',strtotime($enddate));
												?>
                      <dd> - <?php print $event_end_date; ?></dd>
                      <?php
												  endif;
												  if( $startdate == '' && $enddate == '' ):
												?>
                      <dd>All</dd>
                      <?php
													endif;
												?>
                    </dl>
                    <dl>
                      <dt>Categories:</dt>
                      <?php
													if(is_array($event_category) && !in_array(0,$event_category) ):
														print '<dd>';
														$flag = 0;
														foreach($event_category as $key=>$ecat):
															$tax_term = taxonomy_get_term($ecat);
															if($flag) print ', ';
															print $tax_term->name;
															$flag++;
														endforeach;
														print '</dd>';
													else:
												?>
                      <dd>All</dd>
                      <?php endif; ?>
                    </dl>
                    <dl class="location">
                      <dt>Locations:</dt>
                      <?php
													if(is_array($event_location) && !in_array(0,$event_location) ):
														print '<dd>';
														$flag = 0;
														foreach($event_location as $key=>$ecat):
															$tax_term = taxonomy_get_term($ecat);
															if($flag) print ', ';
															print $tax_term->name;
															$flag++;
														endforeach;
														print '</dd>';
													else:
												?>
                      <dd>All</dd>
                      <?php endif; ?>
                    </dl>
                  </div>
                </div>
              </div>
              <div class="filter">
                <?php
											$events_pager = get_paginate_links($events_count->count, $items_per_page);
											print $events_pager;
									?>
                <label for="event-num">Number of Events Per Page</label>
                <select id="event-num" name="count-select" onchange="initSel()" class="sel-gold">
                  <option<?php if($items_per_page == 10) echo ' selected="selected"' ?> value="10">10</option>
                  <option<?php if($items_per_page == 20) echo ' selected="selected"' ?> value="20">20</option>
                  <option<?php if($items_per_page == 30) echo ' selected="selected"' ?> value="30">30</option>
                  <option<?php if($items_per_page == 1) echo ' selected="selected"' ?> value="1">1</option>
                </select>
                <script type="text/javascript">
										  function initSel(){
												  var _form = document.getElementById('filter-events');
												  var _sel = document.getElementById('event-num');
												  if(_form && _sel){
														  _form.ipp.value = _sel.value;
													  	  _form.submit();
												  }
											  }
									</script> 
              </div>
              <?php if($pueblo_events): ?>
              <ul class="events-list">
                <?php foreach($pueblo_events as $key=>$event): ?>
                <?php
											$node_delta = $event->node_data_field_event_date_delta;
											$event = node_load($event->nid);
										?>
                <?php
											$event_url = url('node/'.$event->nid);
											if($node_delta > 0) $event_url = $event_url.'?event_delta='.$node_delta;
										?>
                <li class="even">
                  <div class="holder">
                    <?php
													if(is_array($event->field_event_thumb)):
													 $image = current($event->field_event_thumb);
													 $image_path = $image['filepath'];
													endif;
												?>
                    <div class="photo"> <a href="<?php print $event_url; ?>"><?php print theme('imagecache', 'event_image', $image_path, $title); ?></a> </div>
                    <div class="add-data">
                      <?php	$event_date = $event->field_event_date[$node_delta];
															$event_date_end = $event->field_event_date_end[$node_delta];
															$estart = $event_date['value'];
															$eend = $event_date_end['value'];
															$estart = strtotime($estart);
															$eend = strtotime($eend);
															if($estart && $estart != $eend && isset($eend)) $eventtime = date('ga',$estart). ' - '.date('ga',$eend);
															if(isset($estart) && !isset($eend)) $eventtime = date('ga',$estart);
															$estart = date('F j, Y',$estart);
															$terms = taxonomy_node_get_terms($event);
															$ecat = array();
															foreach( $terms as $eterm ) {
																if( $eterm->vid == 3 ) $elocation = $eterm->name;
															}
															foreach( $terms as $eterm ) {
																if( $eterm->vid == 2 ) $ecat[] = $eterm->name;
															}
													?>
                      <ul>
                        <li><?php print $estart; ?> </li>
                        <?php if($eventtime): ?>
                        <li><?php print $eventtime; ?></li>
                        <?php endif; ?>
                      </ul>
                      <em class="location"><?php print $elocation; ?></em>
                      <?php if($ecat): ?>
                      <dl>
                        <dt>Category:</dt>
                        <?php foreach( $ecat as $key=>$cat_name ): ?>
                        <dd><?php print $cat_name; ?></dd>
                        <?php endforeach; ?>
                      </dl>
                      <?php endif; ?>
                    </div>
                    <div class="caption"> <strong class="title"><a href="<?php print $event_url; ?>"><?php print $event->title; ?></a></strong>
                      <?php
														$event_teaser = current($event->field_event_teaser);
														if($event_teaser):
													?>
                      <p><?php print $event_teaser['value']; ?></p>
                      <?php
														endif;
													?>
                      <span class="more"><a href="<?php print $event_url; ?>">Event Details</a></span> </div>
                  </div>
                </li>
                <?php endforeach; ?>
              </ul>
              <?php endif; ?>
              <div class="filter without-border">
                <?php
											print $events_pager;
									?>
              </div>
            </div>
            <?php
								if($user->uid):
							?>
              <a class="new-event-button bottom" value="Submit An Event »" href="<?php echo base_path() ?>node/add/event">Submit An Event »</a>

                 <?php
								endif;
							?>
</div> 
      <div class="sidebar orange-bar"> 
        <!-- calendar -->
        <?php
						if($left):
					?>
        <div id="block-views-calendar-calendar_block_1" class="clear-block block block-views">
          <div class="content">
            <?php
						  print $left;
					?>
          </div>
        </div>
        <?php
						endif;
					?>
        
        <!-- heading -->
        <div class="heading">
          <h3>Search All Events</h3>
        </div>
        <!-- search form -->
        <form class="search-frm" id="filter-events" method="get" action="<?php print url('node/14'); ?>">
          <fieldset>
            <div class="row">
              <input type="hidden" name="event_action" value="eventsearch" />
              <input type="hidden" name="ipp" value="10" />
              <label class="date" for="start-date">Start Date:</label>
              <input class="text" name="startdate" id="start-date" type="text"<?php if($startdate) print ' value="'.$startdate.'"'; ?> />
            </div>
            <div class="row">
              <label class="date" for="end-date">End Date:</label>
              <input class="text" name="enddate" id="end-date" type="text"<?php if($enddate) print ' value="'.$enddate.'"'; ?> />
            </div>
            <div class="row"> <span class="label">Categories:</span>
              <div class="two-columns">
                <?php
									  $all_terms = custom_get_terms_by_vocabulary(2);
								?>
                <div class="row">
                  <div class="col">
                    <input class="checkbox" id="all" value="0" type="checkbox"<?php if(is_array($event_category)): if(in_array(0,$event_category)) echo ' checked="checked"'; endif; ?> name="event_category[]"/>
                    <label for="all">All</label>
                  </div>
                </div>
                <?php
									if(is_array($all_terms)):
									  $i = 0;
									  foreach($all_terms as $key=>$vterm):
									  if($i % 2 == 0) print '<div class="row">';
								?>
                <div class="col">
                  <input class="checkbox" id="term-<?php print $vterm->tid ?>" name="event_category[]"<?php if(($event_category) && (in_array($vterm->tid,$event_category))): ?> checked="checked"<?php endif ?> value="<?php print $vterm->tid ?>" type="checkbox" />
                  <label for="term-<?php print $vterm->tid ?>"><?php print $vterm->name ?></label>
                </div>
                <?php
									  $i++;
									  if(( $i % 2 == 0 || count($all_terms) == $i )) print '</div>';
									  endforeach;
									endif;
								?>
              </div>
            </div>
            <div class="row"> <span class="label">Locations:</span>
              <div class="two-columns">
                <?php
									  $all_terms = custom_get_terms_by_vocabulary(3);
								?>
                <div class="row">
                  <div class="col">
                    <input class="checkbox" id="all-2" value="0"<?php if(is_array($event_location)): if(in_array(0,$event_location)) echo ' checked="checked"'; endif; ?> type="checkbox" name="event_location[]"/>
                    <label for="all-2">All</label>
                  </div>
                </div>
                <?php
									if(is_array($all_terms)):
									  $i = 0;
									  foreach($all_terms as $key=>$vterm):
									  if(($i % 2 == 0)) print '<div class="row">';
								?>
                <div class="col">
                  <input class="checkbox" id="term-<?php print $vterm->tid ?>" name="event_location[]"<?php if(($event_location) && (in_array($vterm->tid,$event_location))): ?> checked="checked"<?php endif ?> value="<?php print $vterm->tid ?>" type="checkbox" />
                  <label for="term-<?php print $vterm->tid ?>"><?php print $vterm->name ?></label>
                </div>
                <?php
									  $i++;
									  if(( $i % 2 == 0 || count($all_terms) == $i )) print '</div>';
									  endforeach;
									endif;
								?>
              </div>
            </div>
            <div class="row">
              <input class="btn-submit" type="submit" value="Search »" />
            </div>
          </fieldset>
        </form>
      </div>
    </div>
  </div>
  <?php print $breadcrumb; ?> 
  <!-- header -->
  <div id="header">
    <div class="bg">
      <?php
                    if(is_array($node->field_header_image)){
                     $image = current($node->field_header_image);
                     $image_path = $image['filepath'];
                     print theme('imagecache', 'header_image', $image_path, $title) ;
                    } else {
                      print '<img src="'.  base_path() . path_to_theme() .'/images/visual2.jpg" alt="image description" class="visual-image" />';
                    }
                ?>
      <?php print $header; ?> </div>
  </div>
  <!-- headline -->
  <div id="headline"> 
    <!-- search form -->
    <?php if ($search_box): ?>
    <div class="search"><?php print $search_box ?></div>
    <?php endif; ?>
    <!-- top menu -->
    <ul class="top-menu">
      <!-- <li><a href="#"><span>My pueblo</span></a></li> -->
      <li><a href="/jobs">Jobs</a></li>
      <li><a href="/search">Search:</a></li>
    </ul>
  </div>
</div>
<!-- footer -->
<div id="footer">
  <div class="wrapper"> <?php print $footer; ?> </div>
</div>
<?php print $closure ?>
</body>
</html>
