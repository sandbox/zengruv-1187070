
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" dir="ltr">

  <head> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
 
<link rel="shortcut icon" href="/sites/default/files/pueblocolorado_favicon_0.gif" type="image/x-icon" /> 
    <title>Visit Pueblo | Pueblo.org | Pueblo, Colorado</title>

    <link type="text/css" rel="stylesheet" media="all" href="/sites/all/modules/admin_menu/admin_menu.css?I" /> 
<link type="text/css" rel="stylesheet" media="all" href="/modules/aggregator/aggregator.css?I" /> 
<link type="text/css" rel="stylesheet" media="all" href="/modules/node/node.css?I" /> 
<link type="text/css" rel="stylesheet" media="all" href="/modules/poll/poll.css?I" /> 
<link type="text/css" rel="stylesheet" media="all" href="/modules/system/defaults.css?I" /> 
<link type="text/css" rel="stylesheet" media="all" href="/modules/system/system.css?I" /> 
<link type="text/css" rel="stylesheet" media="all" href="/modules/system/system-menus.css?I" /> 
<link type="text/css" rel="stylesheet" media="all" href="/modules/user/user.css?I" /> 
<link type="text/css" rel="stylesheet" media="all" href="/sites/all/modules/cck/theme/content-module.css?I" /> 
<link type="text/css" rel="stylesheet" media="all" href="/sites/all/modules/ckeditor/ckeditor.css?I" /> 
<link type="text/css" rel="stylesheet" media="all" href="/sites/all/modules/ctools/css/ctools.css?I" /> 
<link type="text/css" rel="stylesheet" media="all" href="/sites/all/modules/date/date.css?I" /> 
<link type="text/css" rel="stylesheet" media="all" href="/sites/all/modules/date/date_popup/themes/datepicker.css?I" /> 
<link type="text/css" rel="stylesheet" media="all" href="/sites/all/modules/date/date_popup/themes/jquery.timeentry.css?I" /> 
<link type="text/css" rel="stylesheet" media="all" href="/sites/all/modules/filefield/filefield.css?I" /> 
<link type="text/css" rel="stylesheet" media="all" href="/sites/all/modules/tagadelic/tagadelic.css?I" /> 
<link type="text/css" rel="stylesheet" media="all" href="/modules/forum/forum.css?I" /> 
<link type="text/css" rel="stylesheet" media="all" href="/sites/all/modules/admin/includes/admin.toolbar.base.css?I" /> 
<link type="text/css" rel="stylesheet" media="all" href="/sites/all/modules/admin/includes/admin.toolbar.css?I" /> 
<link type="text/css" rel="stylesheet" media="all" href="/sites/all/modules/admin/includes/admin.menu.css?I" /> 
<link type="text/css" rel="stylesheet" media="all" href="/sites/all/modules/calendar/calendar.css?I" /> 
<link type="text/css" rel="stylesheet" media="all" href="/misc/farbtastic/farbtastic.css?I" /> 
<link type="text/css" rel="stylesheet" media="all" href="/sites/all/modules/extlink/extlink.css?I" /> 
<link type="text/css" rel="stylesheet" media="all" href="/sites/all/modules/cck/modules/fieldgroup/fieldgroup.css?I" /> 
<link type="text/css" rel="stylesheet" media="all" href="/sites/all/modules/views/css/views.css?I" /> 
<link type="text/css" rel="stylesheet" media="all" href="/sites/all/themes/pueblocolorado/css/all.css?I" /> 
<link type="text/css" rel="stylesheet" media="all" href="/sites/all/themes/pueblocolorado/css/form.css?I" /> 
<link type="text/css" rel="stylesheet" media="all" href="/sites/all/themes/pueblocolorado/style.css?I" /> 
<link type="text/css" rel="stylesheet" media="print" href="/sites/all/themes/pueblocolorado/css/print.css?I" /> 
    <script type="text/javascript" src="/sites/all/modules/jquery_update/replace/jquery.min.js?I"></script> 
<script type="text/javascript" src="/misc/drupal.js?I"></script> 
<script type="text/javascript" defer="defer" src="/sites/all/modules/admin_menu/admin_menu.js?I"></script> 
<script type="text/javascript" src="/sites/all/modules/admin/includes/jquery.cookie.js?I"></script> 
<script type="text/javascript" src="/sites/all/modules/admin/includes/jquery.drilldown.js?I"></script> 
<script type="text/javascript" src="/sites/all/modules/admin/includes/admin.toolbar.js?I"></script> 
<script type="text/javascript" src="/sites/all/modules/admin/includes/admin.menu.js?I"></script> 
<script type="text/javascript" src="/sites/all/modules/extlink/extlink.js?I"></script> 
<script type="text/javascript" src="/sites/all/themes/pueblocolorado/js/jquery-ui-1.8.4.custom.min.js?I"></script> 
<script type="text/javascript" src="/sites/all/themes/pueblocolorado/js/jquery.accordion.js?I"></script> 
<script type="text/javascript" src="/sites/all/themes/pueblocolorado/js/cufon.js?I"></script> 
<script type="text/javascript" src="/sites/all/themes/pueblocolorado/js/cufon-fonts.js?I"></script> 
<script type="text/javascript" src="/sites/all/themes/pueblocolorado/js/form.js?I"></script> 
<script type="text/javascript" src="/sites/all/themes/pueblocolorado/js/jquery.gallery.js?I"></script> 
<script type="text/javascript" src="/sites/all/themes/pueblocolorado/js/accordion.js?I"></script> 
<script type="text/javascript" src="/sites/all/themes/pueblocolorado/js/jquery.main.js?I"></script> 
<script type="text/javascript"> 
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, { "basePath": "/", "admin_menu": { "margin_top": 1, "position_fixed": 1 }, "googleanalytics": { "trackOutgoing": 1, "trackMailto": 1, "trackDownload": 1, "trackDownloadExtensions": "7z|aac|arc|arj|asf|asx|avi|bin|csv|doc|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls|xml|z|zip" }, "extlink": { "extTarget": 0, "extClass": 0, "extSubdomains": 1, "extExclude": "", "extInclude": "", "extAlert": 0, "extAlertText": "This link will take you to an external web site. We are not responsible for their content.", "mailtoClass": 0 }, "activePath": "/" });
//--><!]]>
</script> 
<script type="text/javascript"> 
<!--//--><![CDATA[//><!--
$.fn.showIf = function( show, arg ) {
       return this[ show ? 'show' : 'hide' ]( arg );
     };
     $().ready(function() {
       $('#edit-profile-country').change(function() {
         var country = $(this).val();
         $('#state').showIf( country == 'US' );
         $('#province').showIf( country == 'CA' );
       }).trigger('change');
     }); 
//--><!]]>
</script> 
    <!--[if lt IE 7]>

      <script type="text/javascript" src="/sites/all/themes/pueblocolorado/js/ie-png.js"></script>    <![endif]-->

    <!--[if lt IE 8]>

      <link type="text/css" rel="stylesheet" media="all" href="/sites/all/themes/pueblocolorado/css/ie.css" />    <![endif]-->

  </head>

    <body class="default">

	<div class="wrapper">

		<!-- logo -->

		<strong class="logo"><a href="http://pueblo.org">Pueblo, Colorado.</a></strong>

		<img src="/sites/all/themes/pueblocolorado/images/logo-print.gif" alt="PuebloColorado" class="hidden" />

			<!-- main -->

			<div id="main" class="threecol-main">

				<div class="main-bg">

				  <div class="right-twocol">

					  <div class="center-col">

						  <div class="text-wrapper">

							  <div class="holder">

								  								  <h1>Visit Pueblo</h1>								  <div id="tabs-wrapper" class="clear-block">								  <ul class="tabs primary"><li class="active" ><a href="/visit" class="active">View</a></li> 
<li ><a href="/node/3/edit">Edit</a></li> 
<li ><a href="/node/3/devel">Devel</a></li> 
</ul></div>								  								  								  								  <div id="node-3" class="node"> 
 
 
 
  
  <div class="content clear-block"> 
    <p> 
	Whether you are visiting for fun or planning an event in Pueblo, you&#39;ve made the right choice. Exceptional dining and shopping, first-class facilities, wonderful art scene, recreational activities, wide choice of accommodations, extraordinary service ... it&#39;s all here in Pueblo.</p> 
<p> 
	Let the Greater Pueblo Chamber of Commerce be your partner in planning. They have everything you need to make your visit or event memorable and successful. Send an email at info@pueblochamber.org or call (800) 233-3446.</p> 
  </div> 
 
  <div class="clear-block"> 
    <div class="meta"> 
        </div> 
 
      </div> 
 
</div> 
							  </div>

							  <div class="holder">

								  <span class="horizontal-divider">&nbsp;</span>

							  </div>

							          <div class="holder post single-post"> 
<h2>Pueblo Activities</h2> 
        <div class="photo-col"> 
        <div class="photo"> 
            <div class="bg1"> 
                <div class="bg2"> 
                    <div class="bg3"> 
                        <a href="/node/58"><img src="http://pueblo.org/sites/default/files/imagecache/big_activity_thumb/Ride-by-reflection-1-14.jpg" alt="Mountain Biking in Pueblo" title=""  class="imagecache imagecache-big_activity_thumb" width="201" height="135" /></a> 
                    </div> 
                </div> 
            </div> 
        </div> 
    </div> 
    <div class="descr-col"> 
        <div class="holder"> 
            <h3>Mountain Biking in Pueblo</h3> 
            <p><p>

	There is something for every type of mountain biker or hiker in the Pueblo area. From the beginner to the expert, you can find just about any type of trail in the 70 miles of trails in the surrounding area.</p>

</p>            <a href="/node/58" class="lnk-view lnk-view-green">Learn More</a> 
        </div> 
        <ul class="tools"> 
            <li><a href="http://www.activepueblo.net/maps">Maps</a></li>            <li><a href="http://www.activepueblo.net/albums">Cams</a></li>            <li><a href="#">Lodging</a></li>            <li><a href="http://www.activepueblo.net/">Amenities</a></li>        </ul> 
    </div></div> 
  <div class="holder three-col"> 
  <div class="col3 post"> 
        <div class="photo"> 
        <div class="bg1"> 
            <div class="bg2"> 
                <div class="bg3"> 
                    <a href="/node/59"><img src="http://pueblo.org/sites/default/files/imagecache/small_activity_thumb/DSC_7014-2.jpg" alt="Urban Kayaking" title=""  class="imagecache imagecache-small_activity_thumb" width="122" height="76" /></a> 
                </div> 
            </div> 
        </div> 
    </div> 
    <h4>Urban Kayaking</h4> 
    <p><p>

	Covering a half-mile stretch of the Arkansas River, the Pueblo Whitewater Park cuts directly through downtown Pueblo.</p>

</p>    <p><a href="/node/59" class="lnk-view lnk-view-green">Pueblo Whitewater</a></p>  </div> 
  <div class="col3 post"> 
        <div class="photo"> 
        <div class="bg1"> 
            <div class="bg2"> 
                <div class="bg3"> 
                    <a href="/node/60"><img src="http://pueblo.org/sites/default/files/imagecache/small_activity_thumb/3110908AB5544.jpg" alt="Golf" title=""  class="imagecache imagecache-small_activity_thumb" width="122" height="81" /></a> 
                </div> 
            </div> 
        </div> 
    </div> 
    <h4>Golf</h4> 
    <p><p>

	&nbsp;Pueblo is proud to say that our golf courses offer more playable days a year than most others in the state.</p>

</p>    <p><a href="/node/60" class="lnk-view lnk-view-green">Golfing in Pueblo</a></p>  </div> 
  <div class="col3 post"> 
        <div class="photo"> 
        <div class="bg1"> 
            <div class="bg2"> 
                <div class="bg3"> 
                    <a href="/node/61"><img src="http://pueblo.org/sites/default/files/imagecache/small_activity_thumb/attr pueblo zoo photo 1.jpg" alt="The Pueblo Zoo" title=""  class="imagecache imagecache-small_activity_thumb" width="122" height="92" /></a> 
                </div> 
            </div> 
        </div> 
    </div> 
    <h4>The Pueblo Zoo</h4> 
    <p><p>

	Located in City Park, The Pueblo Zoo exhibits more than 350 animals from 122 species, including several endangered.&nbsp;</p>

</p>    <p><a href="/node/61" class="lnk-view lnk-view-green">Pueblo Zoo</a></p>  </div> 
</div> 
						  </div>

					  </div>

					  <div class="aside1">

						  	<div class="heading green-heading"> 
		<h2>Itinerary Ideas</h2> 
	</div> 
          <div class="holder post"> 
    <div class="photo"> 
    <div class="bg1"> 
        <div class="bg2"> 
            <div class="bg3"> 
                <a href=""><img src="http://pueblo.org/sites/default/files/imagecache/2col_right_story/photo10.jpg" alt="Family Fun Itinerary" title=""  class="imagecache imagecache-2col_right_story" width="174" height="108" /></a> 
            </div> 
        </div> 
    </div> 
</div> 
<h3>Family Fun Itinerary</h3> 
<p><p>

	Whether you are here for a day or a weekend, there is so much to see and do. Let us help you organize your visit and make the most of your time with us!</p>

</p> 
<ul class="link-list link-list-green"> 
    <li><a href="/node/57">Learn More</a></li> 
    <li><a href="/node/83">See More Itinerary Ideas</a></li> 
</ul></div> 
  							  									<div class="heading orange-heading">

										<h3>Pueblo Events</h3>

									</div>

									        	<div class="post"> 
		<em class="date">THIS WEEK</em> 
<h3><a href="/content/city-council-meeting">City Council Meeting</a></h3> 
<div class="hold"> 
    <div class="photo-col"> 
        <div class="photo"> 
            <div class="bg1"> 
                <div class="bg2"> 
                    <div class="bg3"> 
                        <a href="/content/city-council-meeting"><img src="http://pueblo.org/sites/default/files/imagecache/event_image/CityHall1.jpg" alt="City Council Meeting" title=""  class="imagecache imagecache-event_image" width="80" height="60" /></a> 
                    </div> 
                </div> 
            </div> 
        </div> 
    </div> 
    <div class="descr-col"> 
        <p><p>

	Meetings are held in Council Chambers on the second floor of City Hall.</p>

</p> 
    </div> 
</div> 
<p><a href="/content/city-council-meeting" class="lnk-view lnk-view-orange">Event Details</a></p>	</div> 
	<div class="holder"> 
		<span class="horizontal-divider">&nbsp;</span> 
	</div> 
	<div class="post"> 
		<em class="date">THIS WEEK</em> 
<h3><a href="/node/39">Center Stage presents the Broadmoor Pops Orchestra</a></h3> 
<div class="hold"> 
    <div class="photo-col"> 
        <div class="photo"> 
            <div class="bg1"> 
                <div class="bg2"> 
                    <div class="bg3"> 
                        <a href="/node/39"><img src="http://pueblo.org/sites/default/files/imagecache/event_image/CenterStageLogo_thumb.jpg" alt="Center Stage presents the Broadmoor Pops Orchestra" title=""  class="imagecache imagecache-event_image" width="67" height="67" /></a> 
                    </div> 
                </div> 
            </div> 
        </div> 
    </div> 
    <div class="descr-col"> 
        <p><p>

	Composer and arranger Ken Miller conducts the Broadmoor Pops Orchestra&rsquo;s string and rhythm sections featuring Broadmoor vocalist Lila Mori.</p>

</p> 
    </div> 
</div> 
<p><a href="/node/39" class="lnk-view lnk-view-orange">Event Details</a></p>	</div> 
	<div class="holder"> 
		<span class="horizontal-divider">&nbsp;</span> 
	</div> 
  <ul class="link-list"> 
<li><a href="#">View All June Events</a></li> 
<li><a href="#">Full Events Calendar</a></li> 
</ul>							  

					  </div>

				  </div>

				  <div class="sidebar green-bar">

					  	<div class="heading green-heading"> 
		<h2>Navigate 2</h2> 
	</div> 
<div class="subnav"> 
<ul class="subnav-list tree-menu menu-2"> 
<li class="item-1-1 first submenu"><a href="/government">Government</a><div class="opened-box slide"><div class="top">&nbsp;</div><ul class="level-2"><li class="item-2-1 first"><a href="/node/4">Property Search</a></li> 
<li class="item-2-2"><a href="http://pueblo.org/sites/?p=http://bit.ly/cCbbGq&amp;t=Pay Parking Ticket">Pay Parking Ticket</a></li> 
<li class="item-2-3 submenu"><a href="/node/6">Elected Officials</a><li class="item-3-1 first last"><a href="/government/elected/county/bocc">Board of County Commissioners</a></li> 
</li> 
<li class="item-2-4"><a href="/node/7">Emergency Services</a></li> 
<li class="item-2-5"><a href="/node/8">Ordinances and Codes</a></li> 
<li class="item-2-6 last"><a href="/government/county">Pueblo County Government</a></li> 
</ul><div class="bottom">&nbsp;</div></div></li> 
<li class="item-1-2 activ submenu expand"><a href="/visit" class="active">Visit Pueblo</a><div class="opened-box slide"><div class="top">&nbsp;</div><ul class="level-2"><li class="item-2-1 first submenu"><a href="/visit/activities">Activities</a><li class="item-3-1 first"><a href="/visit/activities/family">Family Activities</a></li> 
<li class="item-3-2"><a href="/visit/activities/golf">Golf</a></li> 
<li class="item-3-3"><a href="/visit/activities/biking-hiking">Biking &amp; Hiking</a></li> 
<li class="item-3-4"><a href="/visit/activities/kayak">Kayaking &amp; Rafting</a></li> 
<li class="item-3-5"><a href="/visit/activities/outdoor-sporting">Camping, Fishing &amp; Hunting</a></li> 
<li class="item-3-6 last"><a href="/visit/activities/organized">Organized Recreation</a></li> 
</li> 
<li class="item-2-2"><a href="/visit/what-to-do">What to Do?</a></li> 
<li class="item-2-3 submenu"><a href="/visit/points-of-interest">Points of Interest</a><li class="item-3-1 first"><a href="/monuments">Monuments and Landmarks</a></li> 
<li class="item-3-2"><a href="/museums">Museums &amp; Galleries</a></li> 
<li class="item-3-3 last"><a href="/parks">Parks</a></li> 
</li> 
<li class="item-2-4"><a href="/node/11">Daily Itinerary</a></li> 
<li class="item-2-5"><a href="/node/12">Events Calendar</a></li> 
<li class="item-2-6"><a href="/visit/conferences">Conferences</a></li> 
<li class="item-2-7 last"><a href="/visit/lodging">Lodging &amp; Amenities</a></li> 
</ul><div class="bottom">&nbsp;</div></div></li> 
<li class="item-1-3 submenu"><a href="/node/14">Events</a><div class="opened-box slide"><div class="top">&nbsp;</div><ul class="level-2"><li class="item-2-1 first"><a href="/node/15">Arts</a></li> 
<li class="item-2-2"><a href="/events/sports">Sports</a></li> 
<li class="item-2-3"><a href="/node/17">Business</a></li> 
<li class="item-2-4"><a href="/node/18">Family</a></li> 
<li class="item-2-5"><a href="/node/19">Conferences</a></li> 
<li class="item-2-6"><a href="/node/20">Submit an Event</a></li> 
<li class="item-2-7 last"><a href="/node/21">Search Events</a></li> 
</ul><div class="bottom">&nbsp;</div></div></li> 
<li class="item-1-4 submenu"><a href="/living">Living in Pueblo</a><div class="opened-box slide"><div class="top">&nbsp;</div><ul class="level-2"><li class="item-2-1 first"><a href="/node/24">Flavor of Pueblo</a></li> 
<li class="item-2-2 submenu"><a href="/living/community">Community</a><li class="item-3-1 first"><a href="/community/education">Education</a></li> 
<li class="item-3-2 last"><a href="/community/transportation">Transportation</a></li> 
</li> 
<li class="item-2-3"><a href="/node/28">Working in Pueblo</a></li> 
<li class="item-2-4"><a href="/node/27">Business</a></li> 
<li class="item-2-5"><a href="/node/26">Sustainability Initiatives</a></li> 
<li class="item-2-6 last"><a href="/economicdevelopment">Economic Development</a></li> 
</ul><div class="bottom">&nbsp;</div></div></li> 
<li class="item-1-5 last submenu"><a href="/history">History</a><div class="opened-box slide"><div class="top">&nbsp;</div><ul class="level-2"><li class="item-2-1 first"><a href="/node/31">History Timeline</a></li> 
<li class="item-2-2"><a href="/node/32">Pueblo&#039;s Business History</a></li> 
<li class="item-2-3"><a href="/node/33">Search History</a></li> 
<li class="item-2-4"><a href="/node/34">Photo Archive</a></li> 
<li class="item-2-5 last"><a href="/node/35">Museums &amp; Historical Landmarks</a></li> 
</ul><div class="bottom">&nbsp;</div></div></li> 
</ul> 
</div> 
<div class="heading"> 
<h3>Pueblo Gallery</h3> 
</div> 
<!-- side box --> 
<div class="side-box"> 
<div class="text"> 
<div class="sidebar-photo"> 
<span class="photo-t">&nbsp;</span> 
<div class="photo-m"> 
<img src="/sites/all/themes/pueblocolorado/images/photo11.jpg" alt="photo" /> 
</div> 
<span class="photo-b">&nbsp;</span> 
</div> 
<h3>Explore Pueblo</h3> 
<p>Check out photo, videos and our live web cams from all around Pueblo.</p> 
<ul class="tools"> 
<li><a href="#">Photos</a></li> 
<li><a href="#">Video</a></li> 
<li><a href="#">Cams</a></li> 
</ul> 
</div> 
</div>	<div class="heading green-heading"> 
		<h2>Visit Pueblo</h2> 
	</div> 
<div class="subnav"> 
<ul class="subnav-list tree-menu menu-4"> 
<li class="item-1-1 first submenu"><a href="/visit/activities">Activities</a><div class="opened-box slide"><div class="top">&nbsp;</div><ul class="level-2"><li class="item-2-1 first"><a href="/visit/activities/family">Family Activities</a></li> 
<li class="item-2-2"><a href="/visit/activities/golf">Golf</a></li> 
<li class="item-2-3"><a href="/visit/activities/biking-hiking">Biking &amp; Hiking</a></li> 
<li class="item-2-4"><a href="/visit/activities/kayak">Kayaking &amp; Rafting</a></li> 
<li class="item-2-5"><a href="/visit/activities/outdoor-sporting">Camping, Fishing &amp; Hunting</a></li> 
<li class="item-2-6 last"><a href="/visit/activities/organized">Organized Recreation</a></li> 
</ul><div class="bottom">&nbsp;</div></div></li> 
<li class="item-1-2"><a href="/visit/what-to-do">What to Do?</a></li> 
<li class="item-1-3 submenu"><a href="/visit/points-of-interest">Points of Interest</a><div class="opened-box slide"><div class="top">&nbsp;</div><ul class="level-2"><li class="item-2-1 first"><a href="/monuments">Monuments and Landmarks</a></li> 
<li class="item-2-2"><a href="/museums">Museums &amp; Galleries</a></li> 
<li class="item-2-3 last"><a href="/parks">Parks</a></li> 
</ul><div class="bottom">&nbsp;</div></div></li> 
<li class="item-1-4"><a href="/node/11">Daily Itinerary</a></li> 
<li class="item-1-5"><a href="/node/12">Events Calendar</a></li> 
<li class="item-1-6"><a href="/visit/conferences">Conferences</a></li> 
<li class="item-1-7 last"><a href="/visit/lodging">Lodging &amp; Amenities</a></li> 
</ul> 
</div> 
				  </div>

				</div>

			</div>

        <div class="breadcrumbs"><ul> <li><a href="/">Home</a> </li> <li> Visit Pueblo </li> </ul></div>		<!-- header -->

		<div id="header">

			<div class="bg">

                <img src="http://pueblo.org/sites/default/files/imagecache/header_image/Greenhorn-II.jpg" alt="Visit Pueblo" title=""  class="imagecache imagecache-header_image" width="940" height="229" />                <div class="navigation"> 
<ul class="tree-menu menu-1"> 
<li class="item-1-1 first submenu"><a href="/government" class="color1"><span>Government</span></a><ul class="level-2 color1"><li class="item-2-1 first"><a href="/node/4">Property Search</a></li> 
<li class="item-2-2"><a href="http://pueblo.org/sites/?p=http://bit.ly/cCbbGq&amp;t=Pay Parking Ticket">Pay Parking Ticket</a></li> 
<li class="item-2-3 submenu"><a href="/node/6">Elected Officials</a></li> 
<li class="item-2-4"><a href="/node/7">Emergency Services</a></li> 
<li class="item-2-5"><a href="/node/8">Ordinances and Codes</a></li> 
<li class="item-2-6 last"><a href="/government/county">Pueblo County Government</a></li> 
</ul> 
</li> 
<li class="item-1-2 activ submenu expand"><a href="/visit" class="color2"><span>Visit Pueblo</span></a><ul class="level-2 color2"><li class="item-2-1 first submenu"><a href="/visit/activities">Activities</a></li> 
<li class="item-2-2"><a href="/visit/what-to-do">What to Do?</a></li> 
<li class="item-2-3 submenu"><a href="/visit/points-of-interest">Points of Interest</a></li> 
<li class="item-2-4"><a href="/node/11">Daily Itinerary</a></li> 
<li class="item-2-5"><a href="/node/12">Events Calendar</a></li> 
<li class="item-2-6"><a href="/visit/conferences">Conferences</a></li> 
<li class="item-2-7 last"><a href="/visit/lodging">Lodging &amp; Amenities</a></li> 
</ul> 
</li> 
<li class="item-1-3 submenu"><a href="/node/14" class="color3"><span>Events</span></a><ul class="level-2 color3"><li class="item-2-1 first"><a href="/node/15">Arts</a></li> 
<li class="item-2-2"><a href="/events/sports">Sports</a></li> 
<li class="item-2-3"><a href="/node/17">Business</a></li> 
<li class="item-2-4"><a href="/node/18">Family</a></li> 
<li class="item-2-5"><a href="/node/19">Conferences</a></li> 
<li class="item-2-6"><a href="/node/20">Submit an Event</a></li> 
<li class="item-2-7 last"><a href="/node/21">Search Events</a></li> 
</ul> 
</li> 
<li class="item-1-4 submenu"><a href="/living" class="color4"><span>Living in Pueblo</span></a><ul class="level-2 color4"><li class="item-2-1 first"><a href="/node/24">Flavor of Pueblo</a></li> 
<li class="item-2-2 submenu"><a href="/living/community">Community</a></li> 
<li class="item-2-3"><a href="/node/28">Working in Pueblo</a></li> 
<li class="item-2-4"><a href="/node/27">Business</a></li> 
<li class="item-2-5"><a href="/node/26">Sustainability Initiatives</a></li> 
<li class="item-2-6 last"><a href="/economicdevelopment">Economic Development</a></li> 
</ul> 
</li> 
<li class="item-1-5 last submenu"><a href="/history" class="color5"><span>History</span></a><ul class="level-2 color5"><li class="item-2-1 first"><a href="/node/31">History Timeline</a></li> 
<li class="item-2-2"><a href="/node/32">Pueblo&#039;s Business History</a></li> 
<li class="item-2-3"><a href="/node/33">Search History</a></li> 
<li class="item-2-4"><a href="/node/34">Photo Archive</a></li> 
<li class="item-2-5 last"><a href="/node/35">Museums &amp; Historical Landmarks</a></li> 
</ul> 
</li> 
</ul> 
</div> 
			</div>

		</div>

		<!-- headline -->

		<div id="headline">

			<!-- search form -->

            <div class="search"><form action="/visit"  accept-charset="UTF-8" method="post" id="search-theme-form"> 
<div><div id="search" class="container-inline"> 
  <div class="form-item" id="edit-search-theme-form-1-wrapper"> 
 <label for="edit-search-theme-form-1">Search this site: </label> 
 <input type="text" maxlength="128" name="search_theme_form" id="edit-search-theme-form-1" size="15" value="" title="Enter the terms you wish to search for." class="form-text" /> 
</div> 
<input type="submit" name="op" id="edit-submit" value="Search"  class="form-submit" /> 
<input type="hidden" name="form_build_id" id="form-fbfcb820b2cabcfd00f7be49077854ad" value="form-fbfcb820b2cabcfd00f7be49077854ad"  /> 
<input type="hidden" name="form_token" id="edit-search-theme-form-form-token" value="14ee4b53011d0d80d6f0fefb29c84705"  /> 
<input type="hidden" name="form_id" id="edit-search-theme-form" value="search_theme_form"  /> 
</div> 
 
</div></form> 
</div>			<!-- top menu -->

			<ul class="top-menu">
<!-- <li><a href="#"><span>My pueblo</span></a></li> -->
<li><a href="/jobs">Jobs</a></li>
<li><a href="/search">Search:</a></li>
</ul>

		</div>

	</div>

	<!-- footer -->

	<div id="footer">

		<div class="wrapper">

						<ul> 
				<li><a href="#">Contact</a></li> 
				<li><a href="#">Calendar of Events</a></li> 
				<li><a href="#">Terms &amp; Conditions</a></li> 
			</ul> 
			<p>&copy;Pueblo 2010</p>		</div>

	</div>

  <div id="admin-menu"> 
<ul><li class="expandable admin-menu-icon"><a href="/"><img class="admin-menu-icon" src="/sites/default/files/pueblocolorado_favicon_0.gif" width="16" height="16" alt="Home" /></a> 
<ul><li class="expandable"><a href="/admin_menu/flush-cache?destination=node%2F3">Flush all caches</a> 
<ul><li><a href="/admin_menu/flush-cache/admin_menu?destination=node%2F3">Administration menu</a></li><li><a href="/admin_menu/flush-cache/cache?destination=node%2F3">Cache tables</a></li><li><a href="/admin_menu/flush-cache/menu?destination=node%2F3">Menu</a></li><li><a href="/admin_menu/flush-cache/requisites?destination=node%2F3">Page requisites</a></li><li><a href="/admin_menu/flush-cache/theme?destination=node%2F3">Theme registry</a></li></ul></li><li><a href="/devel/variable?destination=node%2F3">Variable editor</a></li><li><a href="/admin/reports/status/run-cron?destination=node%2F3">Run cron</a></li><li><a href="/update.php">Run updates</a></li><li><a href="/admin_menu/toggle-modules?destination=node%2F3">Disable developer modules</a></li><li class="expandable"><a href="http://drupal.org">Drupal.org</a> 
<ul><li><a href="http://drupal.org/project/issues/drupal">Drupal issue queue</a></li><li><a href="http://drupal.org/project/issues/adminrole">Admin Role issue queue</a></li><li><a href="http://drupal.org/project/issues/admin">Admin issue queue</a></li><li><a href="http://drupal.org/project/issues/admin_menu">Administration menu issue queue</a></li><li><a href="http://drupal.org/project/issues/advanced_profile">Advanced Profile Kit issue queue</a></li><li><a href="http://drupal.org/project/issues/advanced_help">Advanced help issue queue</a></li><li><a href="http://drupal.org/project/issues/backup_migrate">Backup and Migrate issue queue</a></li><li><a href="http://drupal.org/project/issues/better_perms">Better Permissions issue queue</a></li><li><a href="http://drupal.org/project/issues/captcha">CAPTCHA issue queue</a></li><li><a href="http://drupal.org/project/issues/ckeditor">CKEditor issue queue</a></li><li><a href="http://drupal.org/project/issues/calendar">Calendar issue queue</a></li><li><a href="http://drupal.org/project/issues/ctools">Chaos tools issue queue</a></li><li><a href="http://drupal.org/project/issues/check_heavy_ui">Check Heavy UI issue queue</a></li><li><a href="http://drupal.org/project/issues/comment_notify">Comment Notify issue queue</a></li><li><a href="http://drupal.org/project/issues/content_profile">Content Profile issue queue</a></li><li><a href="http://drupal.org/project/issues/cck">Content issue queue</a></li><li><a href="http://drupal.org/project/issues/date">Date issue queue</a></li><li><a href="http://drupal.org/project/issues/devel">Devel issue queue</a></li><li><a href="http://drupal.org/project/issues/domain">Domain Access issue queue</a></li><li><a href="http://drupal.org/project/issues/emfield">Embedded Media Field issue queue</a></li><li><a href="http://drupal.org/project/issues/event_views">Event Views issue queue</a></li><li><a href="http://drupal.org/project/issues/event">Event issue queue</a></li><li><a href="http://drupal.org/project/issues/extlink">External Links issue queue</a></li><li><a href="http://drupal.org/project/issues/fbconnect">Fbconnect issue queue</a></li><li><a href="http://drupal.org/project/issues/feedapi">FeedAPI issue queue</a></li><li><a href="http://drupal.org/project/issues/feeds">Feeds issue queue</a></li><li><a href="http://drupal.org/project/issues/filefield">FileField issue queue</a></li><li><a href="http://drupal.org/project/issues/filter_perms">Filter Permissions issue queue</a></li><li><a href="http://drupal.org/project/issues/gmap">GMap issue queue</a></li><li><a href="http://drupal.org/project/issues/globalredirect">Global Redirect issue queue</a></li><li><a href="http://drupal.org/project/issues/google_analytics">Google Analytics issue queue</a></li><li><a href="http://drupal.org/project/issues/gravatar">Gravatar integration issue queue</a></li><li><a href="http://drupal.org/project/issues/groupadmin">Group Admin issue queue</a></li><li><a href="http://drupal.org/project/issues/htpasswdsync">HTPasswdSync issue queue</a></li><li><a href="http://drupal.org/project/issues/imce_mkdir">IMCE Mkdir issue queue</a></li><li><a href="http://drupal.org/project/issues/imce">IMCE issue queue</a></li><li><a href="http://drupal.org/project/issues/imagefield_crop">Image crop issue queue</a></li><li><a href="http://drupal.org/project/issues/imageapi">ImageAPI issue queue</a></li><li><a href="http://drupal.org/project/issues/imagecache">ImageCache issue queue</a></li><li><a href="http://drupal.org/project/issues/imagefield">ImageField issue queue</a></li><li><a href="http://drupal.org/project/issues/job_scheduler">Job Scheduler issue queue</a></li><li><a href="http://drupal.org/project/issues/location">Location issue queue</a></li><li><a href="http://drupal.org/project/issues/media_flickr">Media: Flickr issue queue</a></li><li><a href="http://drupal.org/project/issues/media_youtube">Media: YouTube issue queue</a></li><li><a href="http://drupal.org/project/issues/menu_breadcrumb">Menu breadcrumb issue queue</a></li><li><a href="http://drupal.org/project/issues/messaging">Messaging issue queue</a></li><li><a href="http://drupal.org/project/issues/node_style">Node style issue queue</a></li><li><a href="http://drupal.org/project/issues/nodewords">Nodewords issue queue</a></li><li><a href="http://drupal.org/project/issues/notifications">Notifications issue queue</a></li><li><a href="http://drupal.org/project/issues/openidadmin">OpenID Admin issue queue</a></li><li><a href="http://drupal.org/project/issues/openidurl">OpenID URL issue queue</a></li><li><a href="http://drupal.org/project/issues/openid_autoreg">OpenID autoregistration issue queue</a></li><li><a href="http://drupal.org/project/issues/og">Organic groups issue queue</a></li><li><a href="http://drupal.org/project/issues/pngfix">PNG Fix issue queue</a></li><li><a href="http://drupal.org/project/issues/page_title">Page Title issue queue</a></li><li><a href="http://drupal.org/project/issues/path_redirect">Path redirect issue queue</a></li><li><a href="http://drupal.org/project/issues/pathauto">Pathauto issue queue</a></li><li><a href="http://drupal.org/project/issues/permission_select">Permission Select issue queue</a></li><li><a href="http://drupal.org/project/issues/profile_blog_info">Profile Blog Information issue queue</a></li><li><a href="http://drupal.org/project/issues/profile_location">Profile Location issue queue</a></li><li><a href="http://drupal.org/project/issues/profile_ops">Profile Operations issue queue</a></li><li><a href="http://drupal.org/project/issues/rpx">RPX issue queue</a></li><li><a href="http://drupal.org/project/issues/realname">RealName issue queue</a></li><li><a href="http://drupal.org/project/issues/sharedblocks">Shared Blocks issue queue</a></li><li><a href="http://drupal.org/project/issues/tagadelic">Tagadelic issue queue</a></li><li><a href="http://drupal.org/project/issues/taxonomy_manager">Taxonomy Manager issue queue</a></li><li><a href="http://drupal.org/project/issues/devel_themer">Theme developer issue queue</a></li><li><a href="http://drupal.org/project/issues/token">Token issue queue</a></li><li><a href="http://drupal.org/project/issues/views_bulk_operations">Views Bulk Operations issue queue</a></li><li><a href="http://drupal.org/project/issues/views">Views issue queue</a></li><li><a href="http://drupal.org/project/issues/twci_weather">Weather (API weather.com) issue queue</a></li><li><a href="http://drupal.org/project/issues/weather">Weather issue queue</a></li><li><a href="http://drupal.org/project/issues/webform">Webform issue queue</a></li><li><a href="http://drupal.org/project/issues/xmlsitemap">XML sitemap issue queue</a></li><li><a href="http://drupal.org/project/issues/xmppframework">XMPP-Framework issue queue</a></li><li><a href="http://drupal.org/project/issues/getid3">getID3() issue queue</a></li><li><a href="http://drupal.org/project/issues/jquery_ui">jQuery UI issue queue</a></li><li><a href="http://drupal.org/project/issues/jquery_update">jQuery Update issue queue</a></li><li><a href="http://drupal.org/project/issues/nws_weather">nws_weather issue queue</a></li><li><a href="http://drupal.org/project/issues/recaptcha">reCAPTCHA issue queue</a></li></ul></li></ul></li><li class="expandable admin-menu-action admin-menu-logout"><a href="/logout">Log out webadmin</a> 
<ul><li><a href="/devel/switch/Rob?destination=node%2F3"><em>Rob</em></a></li><li><a href="/devel/switch/p2h?destination=node%2F3"><em>p2h</em></a></li><li><a href="/devel/switch/webadmin?destination=node%2F3"><em>webadmin</em></a></li><li><a href="/devel/switch/bcuomo?destination=node%2F3">bcuomo</a></li><li><a href="/devel/switch/jbittle?destination=node%2F3">jbittle</a></li><li><a href="/devel/switch/markuson1?destination=node%2F3">markuson1</a></li><li><a href="/devel/switch/mauro?destination=node%2F3">mauro</a></li></ul></li><li class=" admin-menu-action admin-menu-icon admin-menu-users"><a href="/admin/user/user">0 / 1 <img src="/sites/all/modules/admin_menu/images/icon_users.png" width="16" height="15" alt="Current anonymous / authenticated users" title="Current anonymous / authenticated users" /></a></li><li class="expandable"><a href="/admin/content">Content management</a> 
<ul><li class="expandable"><a href="/admin/content/backup_migrate">Backup and Migrate</a> 
<ul><li class="expandable"><a href="/admin/content/backup_migrate/export">Backup</a> 
<ul><li><a href="/admin/content/backup_migrate/export/quick">Quick Backup</a></li><li><a href="/admin/content/backup_migrate/export/advanced">Advanced Backup</a></li></ul></li><li><a href="/admin/content/backup_migrate/restore">Restore</a></li><li class="expandable"><a href="/admin/content/backup_migrate/destination">Destinations</a> 
<ul><li class="expandable"><a href="/admin/content/backup_migrate/destination/list">List Destinations</a> 
<ul><li><a href="/admin/content/backup_migrate/destination/list/delete">Delete Destination</a></li><li><a href="/admin/content/backup_migrate/destination/list/deletefile">Delete File</a></li><li><a href="/admin/content/backup_migrate/destination/list/files">Destination Files</a></li><li><a href="/admin/content/backup_migrate/destination/list/edit">Edit Destination</a></li><li><a href="/admin/content/backup_migrate/destination/list/export">Export Destination</a></li><li><a href="/admin/content/backup_migrate/destination/list/restorefile">Restore from backup</a></li></ul></li><li><a href="/admin/content/backup_migrate/destination/add">Create Destination</a></li></ul></li><li class="expandable"><a href="/admin/content/backup_migrate/profile">Profiles</a> 
<ul><li class="expandable"><a href="/admin/content/backup_migrate/profile/list">List Profiles</a> 
<ul><li><a href="/admin/content/backup_migrate/profile/list/delete">Delete Profile</a></li><li><a href="/admin/content/backup_migrate/profile/list/edit">Edit Profile</a></li><li><a href="/admin/content/backup_migrate/profile/list/export">Export Profile</a></li></ul></li><li><a href="/admin/content/backup_migrate/profile/add">Create Profile</a></li></ul></li><li class="expandable"><a href="/admin/content/backup_migrate/schedule">Schedules</a> 
<ul><li class="expandable"><a href="/admin/content/backup_migrate/schedule/list">List Schedules</a> 
<ul><li><a href="/admin/content/backup_migrate/schedule/list/delete">Delete Schedule</a></li><li><a href="/admin/content/backup_migrate/schedule/list/edit">Edit Schedule</a></li><li><a href="/admin/content/backup_migrate/schedule/list/export">Export Schedule</a></li></ul></li><li><a href="/admin/content/backup_migrate/schedule/add">Create Schedule</a></li></ul></li></ul></li><li class="expandable"><a href="/admin/content/comment">Comments</a> 
<ul><li><a href="/admin/content/comment/new">Published comments</a></li><li><a href="/admin/content/comment/approval">Approval queue</a></li></ul></li><li class="expandable"><a href="/admin/content/node">Content</a> 
<ul><li><a href="/admin/content/node/overview">List</a></li></ul></li><li class="expandable"><a href="/admin/content/types">Content types</a> 
<ul><li><a href="/admin/content/types/list">List</a></li><li><a href="/admin/content/types/add">Add content type</a></li><li class="expandable"><a href="/admin/content/node-type/2col">Edit 2 columns</a> 
<ul><li class="expandable"><a href="/admin/content/node-type/2col/fields">Manage fields</a> 
<ul><li><a href="/admin/content/node-type/2col/fields/field_header_image">Header image</a></li></ul></li><li class="expandable"><a href="/admin/content/node-type/2col/display">Display fields</a> 
<ul><li><a href="/admin/content/node-type/2col/display/basic">Basic</a></li><li><a href="/admin/content/node-type/2col/display/rss">RSS</a></li><li><a href="/admin/content/node-type/2col/display/search">Search</a></li><li><a href="/admin/content/node-type/2col/display/token">Token</a></li></ul></li></ul></li><li class="expandable"><a href="/admin/content/node-type/2col-w-nav">Edit 2 columns with navigation</a> 
<ul><li class="expandable"><a href="/admin/content/node-type/2col-w-nav/fields">Manage fields</a> 
<ul><li><a href="/admin/content/node-type/2col-w-nav/fields/field_header_image">Header image</a></li></ul></li><li class="expandable"><a href="/admin/content/node-type/2col-w-nav/display">Display fields</a> 
<ul><li><a href="/admin/content/node-type/2col-w-nav/display/basic">Basic</a></li><li><a href="/admin/content/node-type/2col-w-nav/display/rss">RSS</a></li><li><a href="/admin/content/node-type/2col-w-nav/display/search">Search</a></li><li><a href="/admin/content/node-type/2col-w-nav/display/token">Token</a></li></ul></li></ul></li><li class="expandable"><a href="/admin/content/node-type/3-columns">Edit 3 columns</a> 
<ul><li class="expandable"><a href="/admin/content/node-type/3-columns/fields">Manage fields</a> 
<ul><li><a href="/admin/content/node-type/3-columns/fields/field_header_image">Header image</a></li></ul></li><li class="expandable"><a href="/admin/content/node-type/3-columns/display">Display fields</a> 
<ul><li><a href="/admin/content/node-type/3-columns/display/basic">Basic</a></li><li><a href="/admin/content/node-type/3-columns/display/rss">RSS</a></li><li><a href="/admin/content/node-type/3-columns/display/search">Search</a></li><li><a href="/admin/content/node-type/3-columns/display/token">Token</a></li></ul></li></ul></li><li class="expandable"><a href="/admin/content/node-type/activities">Edit Activities</a> 
<ul><li class="expandable"><a href="/admin/content/node-type/activities/fields">Manage fields</a> 
<ul><li><a href="/admin/content/node-type/activities/fields/field_amenities">Amenities URL</a></li><li><a href="/admin/content/node-type/activities/fields/field_cams">Cams URL</a></li><li><a href="/admin/content/node-type/activities/fields/field_lodging">Lodging URL</a></li><li><a href="/admin/content/node-type/activities/fields/field_maps">Maps URL</a></li><li><a href="/admin/content/node-type/activities/fields/field_readmore">Read More text</a></li><li><a href="/admin/content/node-type/activities/fields/field_teaser">Teaser</a></li><li><a href="/admin/content/node-type/activities/fields/field_thumb">Thumbnail</a></li></ul></li><li class="expandable"><a href="/admin/content/node-type/activities/display">Display fields</a> 
<ul><li><a href="/admin/content/node-type/activities/display/basic">Basic</a></li><li><a href="/admin/content/node-type/activities/display/rss">RSS</a></li><li><a href="/admin/content/node-type/activities/display/search">Search</a></li><li><a href="/admin/content/node-type/activities/display/token">Token</a></li></ul></li></ul></li><li class="expandable"><a href="/admin/content/node-type/citycounty">Edit City &amp; County Info</a> 
<ul><li class="expandable"><a href="/admin/content/node-type/citycounty/fields">Manage fields</a> 
<ul><li><a href="/admin/content/node-type/citycounty/fields/field_header_image">Header image</a></li><li><a href="/admin/content/node-type/citycounty/fields/field_teaser">Teaser</a></li></ul></li><li class="expandable"><a href="/admin/content/node-type/citycounty/display">Display fields</a> 
<ul><li><a href="/admin/content/node-type/citycounty/display/basic">Basic</a></li><li><a href="/admin/content/node-type/citycounty/display/rss">RSS</a></li><li><a href="/admin/content/node-type/citycounty/display/search">Search</a></li><li><a href="/admin/content/node-type/citycounty/display/token">Token</a></li></ul></li></ul></li><li class="expandable"><a href="/admin/content/node-type/communityvision">Edit Community Vision</a> 
<ul><li class="expandable"><a href="/admin/content/node-type/communityvision/fields">Manage fields</a> 
<ul><li><a href="/admin/content/node-type/communityvision/fields/field_sustainability_url">Sustainability Initiatives URL</a></li><li><a href="/admin/content/node-type/communityvision/fields/field_teaser">Teaser</a></li><li><a href="/admin/content/node-type/communityvision/fields/field_thumb">Thumbnail</a></li></ul></li><li class="expandable"><a href="/admin/content/node-type/communityvision/display">Display fields</a> 
<ul><li><a href="/admin/content/node-type/communityvision/display/basic">Basic</a></li><li><a href="/admin/content/node-type/communityvision/display/rss">RSS</a></li><li><a href="/admin/content/node-type/communityvision/display/search">Search</a></li><li><a href="/admin/content/node-type/communityvision/display/token">Token</a></li></ul></li></ul></li><li class="expandable"><a href="/admin/content/node-type/event">Edit Event</a> 
<ul><li class="expandable"><a href="/admin/content/node-type/event/fields">Manage fields</a> 
<ul><li><a href="/admin/content/node-type/event/fields/field_evnt_calendar_url">Add To Calendar URL</a></li><li><a href="/admin/content/node-type/event/fields/field_event_amenties">Amenties URL</a></li><li><a href="/admin/content/node-type/event/fields/field_event_contact">Contact</a></li><li><a href="/admin/content/node-type/event/fields/field_event_date">Date</a></li><li><a href="/admin/content/node-type/event/fields/field_event_date_end">Date End</a></li><li><a href="/admin/content/node-type/event/fields/field_event_website">Evnt Website URL</a></li><li><a href="/admin/content/node-type/event/fields/field_event_thumb">Image</a></li><li><a href="/admin/content/node-type/event/fields/field_event_lodging">Lodging URL</a></li><li><a href="/admin/content/node-type/event/fields/field_event_map_url">Map URL</a></li><li><a href="/admin/content/node-type/event/fields/field_event_teaser">Teaser</a></li></ul></li><li class="expandable"><a href="/admin/content/node-type/event/display">Display fields</a> 
<ul><li><a href="/admin/content/node-type/event/display/basic">Basic</a></li><li><a href="/admin/content/node-type/event/display/rss">RSS</a></li><li><a href="/admin/content/node-type/event/display/search">Search</a></li><li><a href="/admin/content/node-type/event/display/token">Token</a></li></ul></li></ul></li><li class="expandable"><a href="/admin/content/node-type/fooddining">Edit Food &amp; Dining</a> 
<ul><li class="expandable"><a href="/admin/content/node-type/fooddining/fields">Manage fields</a> 
<ul><li><a href="/admin/content/node-type/fooddining/fields/field_dining_thumb">Image</a></li><li><a href="/admin/content/node-type/fooddining/fields/field_lodging">Lodging URL</a></li><li><a href="/admin/content/node-type/fooddining/fields/field_photos">Photos URL</a></li><li><a href="/admin/content/node-type/fooddining/fields/field_tickets">Tickets URL</a></li></ul></li><li class="expandable"><a href="/admin/content/node-type/fooddining/display">Display fields</a> 
<ul><li><a href="/admin/content/node-type/fooddining/display/basic">Basic</a></li><li><a href="/admin/content/node-type/fooddining/display/rss">RSS</a></li><li><a href="/admin/content/node-type/fooddining/display/search">Search</a></li><li><a href="/admin/content/node-type/fooddining/display/token">Token</a></li></ul></li></ul></li><li class="expandable"><a href="/admin/content/node-type/forum">Edit Forum topic</a> 
<ul><li><a href="/admin/content/node-type/forum/fields">Manage fields</a></li><li class="expandable"><a href="/admin/content/node-type/forum/display">Display fields</a> 
<ul><li><a href="/admin/content/node-type/forum/display/basic">Basic</a></li><li><a href="/admin/content/node-type/forum/display/rss">RSS</a></li><li><a href="/admin/content/node-type/forum/display/search">Search</a></li><li><a href="/admin/content/node-type/forum/display/token">Token</a></li></ul></li></ul></li><li class="expandable"><a href="/admin/content/node-type/itinerary-ideas">Edit Itinerary Ideas</a> 
<ul><li class="expandable"><a href="/admin/content/node-type/itinerary-ideas/fields">Manage fields</a> 
<ul><li><a href="/admin/content/node-type/itinerary-ideas/fields/field_event_teaser">Teaser</a></li><li><a href="/admin/content/node-type/itinerary-ideas/fields/field_thumb">Thumbnail</a></li></ul></li><li class="expandable"><a href="/admin/content/node-type/itinerary-ideas/display">Display fields</a> 
<ul><li><a href="/admin/content/node-type/itinerary-ideas/display/basic">Basic</a></li><li><a href="/admin/content/node-type/itinerary-ideas/display/rss">RSS</a></li><li><a href="/admin/content/node-type/itinerary-ideas/display/search">Search</a></li><li><a href="/admin/content/node-type/itinerary-ideas/display/token">Token</a></li></ul></li></ul></li><li class="expandable"><a href="/admin/content/node-type/lodging">Edit Lodging</a> 
<ul><li class="expandable"><a href="/admin/content/node-type/lodging/fields">Manage fields</a> 
<ul><li><a href="/admin/content/node-type/lodging/fields/field_lodging_date">Date</a></li><li><a href="/admin/content/node-type/lodging/fields/field_header_image">Header image</a></li></ul></li><li class="expandable"><a href="/admin/content/node-type/lodging/display">Display fields</a> 
<ul><li><a href="/admin/content/node-type/lodging/display/basic">Basic</a></li><li><a href="/admin/content/node-type/lodging/display/rss">RSS</a></li><li><a href="/admin/content/node-type/lodging/display/search">Search</a></li><li><a href="/admin/content/node-type/lodging/display/token">Token</a></li></ul></li></ul></li><li class="expandable"><a href="/admin/content/node-type/lodgingdeal">Edit Lodging deal</a> 
<ul><li class="expandable"><a href="/admin/content/node-type/lodgingdeal/fields">Manage fields</a> 
<ul><li><a href="/admin/content/node-type/lodgingdeal/fields/field_lodging_thumb">Image</a></li><li><a href="/admin/content/node-type/lodgingdeal/fields/field_lodging_teaser">Teaser</a></li></ul></li><li class="expandable"><a href="/admin/content/node-type/lodgingdeal/display">Display fields</a> 
<ul><li><a href="/admin/content/node-type/lodgingdeal/display/basic">Basic</a></li><li><a href="/admin/content/node-type/lodgingdeal/display/rss">RSS</a></li><li><a href="/admin/content/node-type/lodgingdeal/display/search">Search</a></li><li><a href="/admin/content/node-type/lodgingdeal/display/token">Token</a></li></ul></li></ul></li><li class="expandable"><a href="/admin/content/node-type/news">Edit News</a> 
<ul><li class="expandable"><a href="/admin/content/node-type/news/fields">Manage fields</a> 
<ul><li><a href="/admin/content/node-type/news/fields/field_header_image">Header image</a></li></ul></li><li class="expandable"><a href="/admin/content/node-type/news/display">Display fields</a> 
<ul><li><a href="/admin/content/node-type/news/display/basic">Basic</a></li><li><a href="/admin/content/node-type/news/display/rss">RSS</a></li><li><a href="/admin/content/node-type/news/display/search">Search</a></li><li><a href="/admin/content/node-type/news/display/token">Token</a></li></ul></li></ul></li><li class="expandable"><a href="/admin/content/node-type/organization">Edit Organization</a> 
<ul><li><a href="/admin/content/node-type/organization/fields">Manage fields</a></li><li class="expandable"><a href="/admin/content/node-type/organization/display">Display fields</a> 
<ul><li><a href="/admin/content/node-type/organization/display/basic">Basic</a></li><li><a href="/admin/content/node-type/organization/display/rss">RSS</a></li><li><a href="/admin/content/node-type/organization/display/search">Search</a></li><li><a href="/admin/content/node-type/organization/display/token">Token</a></li></ul></li></ul></li><li class="expandable"><a href="/admin/content/node-type/org-announce">Edit Organization Announcement</a> 
<ul><li><a href="/admin/content/node-type/org-announce/fields">Manage fields</a></li><li class="expandable"><a href="/admin/content/node-type/org-announce/display">Display fields</a> 
<ul><li><a href="/admin/content/node-type/org-announce/display/basic">Basic</a></li><li><a href="/admin/content/node-type/org-announce/display/rss">RSS</a></li><li><a href="/admin/content/node-type/org-announce/display/search">Search</a></li><li><a href="/admin/content/node-type/org-announce/display/token">Token</a></li></ul></li></ul></li><li class="expandable"><a href="/admin/content/node-type/outdoors">Edit Outdoors</a> 
<ul><li class="expandable"><a href="/admin/content/node-type/outdoors/fields">Manage fields</a> 
<ul><li><a href="/admin/content/node-type/outdoors/fields/field_thumbnail">Thumbnail</a></li><li><a href="/admin/content/node-type/outdoors/fields/field_trail_maps_url">Trail Maps URL</a></li><li><a href="/admin/content/node-type/outdoors/fields/field_web_cams">Web Cams URL</a></li></ul></li><li class="expandable"><a href="/admin/content/node-type/outdoors/display">Display fields</a> 
<ul><li><a href="/admin/content/node-type/outdoors/display/basic">Basic</a></li><li><a href="/admin/content/node-type/outdoors/display/rss">RSS</a></li><li><a href="/admin/content/node-type/outdoors/display/search">Search</a></li><li><a href="/admin/content/node-type/outdoors/display/token">Token</a></li></ul></li></ul></li><li class="expandable"><a href="/admin/content/node-type/page">Edit Page</a> 
<ul><li class="expandable"><a href="/admin/content/node-type/page/fields">Manage fields</a> 
<ul><li><a href="/admin/content/node-type/page/fields/field_header_image">Header image</a></li></ul></li><li class="expandable"><a href="/admin/content/node-type/page/display">Display fields</a> 
<ul><li><a href="/admin/content/node-type/page/display/basic">Basic</a></li><li><a href="/admin/content/node-type/page/display/rss">RSS</a></li><li><a href="/admin/content/node-type/page/display/search">Search</a></li><li><a href="/admin/content/node-type/page/display/token">Token</a></li></ul></li></ul></li><li class="expandable"><a href="/admin/content/node-type/poll">Edit Poll</a> 
<ul><li><a href="/admin/content/node-type/poll/fields">Manage fields</a></li><li class="expandable"><a href="/admin/content/node-type/poll/display">Display fields</a> 
<ul><li><a href="/admin/content/node-type/poll/display/basic">Basic</a></li><li><a href="/admin/content/node-type/poll/display/rss">RSS</a></li><li><a href="/admin/content/node-type/poll/display/search">Search</a></li><li><a href="/admin/content/node-type/poll/display/token">Token</a></li></ul></li></ul></li><li class="expandable"><a href="/admin/content/node-type/profile">Edit Profile</a> 
<ul><li><a href="/admin/content/node-type/profile/fields">Manage fields</a></li><li class="expandable"><a href="/admin/content/node-type/profile/display">Display fields</a> 
<ul><li><a href="/admin/content/node-type/profile/display/basic">Basic</a></li><li><a href="/admin/content/node-type/profile/display/rss">RSS</a></li><li><a href="/admin/content/node-type/profile/display/search">Search</a></li><li><a href="/admin/content/node-type/profile/display/token">Token</a></li></ul></li></ul></li><li class="expandable"><a href="/admin/content/node-type/story">Edit Story</a> 
<ul><li class="expandable"><a href="/admin/content/node-type/story/fields">Manage fields</a> 
<ul><li><a href="/admin/content/node-type/story/fields/field_header_image">Header image</a></li><li><a href="/admin/content/node-type/story/fields/field_story_teaser">Teaser</a></li><li><a href="/admin/content/node-type/story/fields/field_thumb">Thumbnail</a></li></ul></li><li class="expandable"><a href="/admin/content/node-type/story/display">Display fields</a> 
<ul><li><a href="/admin/content/node-type/story/display/basic">Basic</a></li><li><a href="/admin/content/node-type/story/display/rss">RSS</a></li><li><a href="/admin/content/node-type/story/display/search">Search</a></li><li><a href="/admin/content/node-type/story/display/token">Token</a></li></ul></li></ul></li><li class="expandable"><a href="/admin/content/node-type/webform">Edit Webform</a> 
<ul><li><a href="/admin/content/node-type/webform/fields">Manage fields</a></li><li class="expandable"><a href="/admin/content/node-type/webform/display">Display fields</a> 
<ul><li><a href="/admin/content/node-type/webform/display/basic">Basic</a></li><li><a href="/admin/content/node-type/webform/display/rss">RSS</a></li><li><a href="/admin/content/node-type/webform/display/search">Search</a></li><li><a href="/admin/content/node-type/webform/display/token">Token</a></li></ul></li></ul></li><li class="expandable"><a href="/admin/content/node-type/whatsnew">Edit What</a> 
<ul><li class="expandable"><a href="/admin/content/node-type/whatsnew/fields">Manage fields</a> 
<ul><li><a href="/admin/content/node-type/whatsnew/fields/field_header_image">Header image</a></li></ul></li><li class="expandable"><a href="/admin/content/node-type/whatsnew/display">Display fields</a> 
<ul><li><a href="/admin/content/node-type/whatsnew/display/basic">Basic</a></li><li><a href="/admin/content/node-type/whatsnew/display/rss">RSS</a></li><li><a href="/admin/content/node-type/whatsnew/display/search">Search</a></li><li><a href="/admin/content/node-type/whatsnew/display/token">Token</a></li></ul></li></ul></li><li><a href="/admin/content/types/fields">Fields</a></li><li><a href="/admin/content/types/export">Export</a></li><li><a href="/admin/content/types/import">Import</a></li></ul></li><li class="expandable"><a href="/node/add">Create content</a> 
<ul><li><a href="/node/add/2col">2 columns</a></li><li><a href="/node/add/2col-w-nav">2 columns with navigation</a></li><li><a href="/node/add/3-columns">3 columns</a></li><li><a href="/node/add/activities">Activities</a></li><li><a href="/node/add/citycounty">City &amp; County Info</a></li><li><a href="/node/add/communityvision">Community Vision</a></li><li><a href="/node/add/event">Event</a></li><li><a href="/node/add/fooddining">Food &amp; Dining</a></li><li><a href="/node/add/forum">Forum topic</a></li><li><a href="/node/add/itinerary-ideas">Itinerary Ideas</a></li><li><a href="/node/add/lodging">Lodging</a></li><li><a href="/node/add/lodgingdeal">Lodging deal</a></li><li><a href="/node/add/news">News</a></li><li><a href="/node/add/organization">Organization</a></li><li><a href="/node/add/org-announce">Organization Announcement</a></li><li><a href="/node/add/outdoors">Outdoors</a></li><li><a href="/node/add/page">Page</a></li><li><a href="/node/add/poll">Poll</a></li><li><a href="/node/add/profile">Profile</a></li><li><a href="/node/add/story">Story</a></li><li><a href="/node/add/webform">Webform</a></li><li><a href="/node/add/whatsnew">What</a></li></ul></li><li class="expandable"><a href="/admin/content/date/tools">Date Tools</a> 
<ul><li><a href="/admin/content/date/tools/about">About</a></li></ul></li><li class="expandable"><a href="/admin/content/emfield">Embedded Media Field configuration</a> 
<ul><li><a href="/admin/content/emfield/media">General</a></li><li><a href="/admin/content/emfield/emvideo">Videos</a></li></ul></li><li class="expandable"><a href="/admin/content/aggregator">Feed aggregator</a> 
<ul><li><a href="/admin/content/aggregator/list">List</a></li><li><a href="/admin/content/aggregator/add/category">Add category</a></li><li><a href="/admin/content/aggregator/add/feed">Add feed</a></li><li><a href="/admin/content/aggregator/settings">Settings</a></li></ul></li><li class="expandable"><a href="/admin/content/feed">Feeds</a> 
<ul><li><a href="/admin/content/feed/list">List</a></li><li><a href="/admin/content/feed/export_opml">Export all feeds as OPML</a></li><li><a href="/admin/content/feed/import_opml">Import OPML</a></li></ul></li><li class="expandable"><a href="/admin/content/forum">Forums</a> 
<ul><li><a href="/admin/content/forum/list">List</a></li><li><a href="/admin/content/forum/add/container">Add container</a></li><li><a href="/admin/content/forum/add/forum">Add forum</a></li><li><a href="/admin/content/forum/settings">Settings</a></li></ul></li><li class="expandable"><a href="/admin/content/nodewords">Meta tags</a> 
<ul><li><a href="/admin/content/nodewords/settings">Settings</a></li><li class="expandable"><a href="/admin/content/nodewords/meta-tags">Default and specific meta tags</a> 
<ul><li><a href="/admin/content/nodewords/meta-tags/default">Default values</a></li><li><a href="/admin/content/nodewords/meta-tags/errorpage_403">Error 403 page</a></li><li><a href="/admin/content/nodewords/meta-tags/errorpage_404">Error 404 page</a></li><li><a href="/admin/content/nodewords/meta-tags/frontpage">Front page</a></li><li><a href="/admin/content/nodewords/meta-tags/tracker">Tracker pages</a></li><li><a href="/admin/content/nodewords/meta-tags/other">Other pages</a></li></ul></li></ul></li><li><a href="/admin/content/page_title">Page titles</a></li><li><a href="/admin/content/node-settings">Post settings</a></li><li><a href="/admin/content/rss-publishing">RSS publishing</a></li><li class="expandable"><a href="/admin/content/taxonomy">Taxonomy</a> 
<ul><li><a href="/admin/content/taxonomy/list">List</a></li><li><a href="/admin/content/taxonomy/add/vocabulary">Add vocabulary</a></li></ul></li><li><a href="/admin/content/taxonomy_manager">Taxonomy Manager</a></li><li><a href="/admin/content/webform">Webforms</a></li></ul></li><li class="expandable"><a href="/admin/build">Site building</a> 
<ul><li class="expandable"><a href="/admin/build/block">Blocks</a> 
<ul><li class="expandable"><a href="/admin/build/block/list">List</a></li><li><a href="/admin/build/block/add">Add block</a></li></ul></li><li class="expandable"><a href="/admin/build/contact">Contact form</a> 
<ul><li><a href="/admin/build/contact/list">List</a></li><li><a href="/admin/build/contact/add">Add category</a></li><li><a href="/admin/build/contact/settings">Settings</a></li></ul></li><li class="expandable"><a href="/admin/build/domain">Domains</a> 
<ul><li><a href="/admin/build/domain/view">Domain list</a></li><li><a href="/admin/build/domain/settings">Settings</a></li><li><a href="/admin/build/domain/create">Create domain record</a></li><li><a href="/admin/build/domain/advanced">Node settings</a></li><li><a href="/admin/build/domain/batch">Batch updating</a></li><li><a href="/admin/build/domain/roles">User defaults</a></li></ul></li><li class="expandable"><a href="/admin/build/imagecache">ImageCache</a> 
<ul><li><a href="/admin/build/imagecache/list">List</a></li><li><a href="/admin/build/imagecache/add">Add new preset</a></li></ul></li><li class="expandable"><a href="/admin/build/menu">Menus</a> 
<ul><li><a href="/admin/build/menu/list">List menus</a></li><li><a href="/admin/build/menu/add">Add menu</a></li><li><a href="/admin/build/menu/settings">Settings</a></li></ul></li><li class="expandable"><a href="/admin/build/modules">Modules</a> 
<ul><li><a href="/admin/build/modules/list">List</a></li><li><a href="/admin/build/modules/uninstall">Uninstall</a></li></ul></li><li class="expandable"><a href="/admin/build/themes">Themes</a> 
<ul><li><a href="/admin/build/themes/select">List</a></li><li class="expandable"><a href="/admin/build/themes/settings">Configure</a> 
<ul><li><a href="/admin/build/themes/settings/global">Global settings</a></li></ul></li></ul></li><li class="expandable"><a href="/admin/build/translate">Translate interface</a> 
<ul><li><a href="/admin/build/translate/overview">Overview</a></li><li><a href="/admin/build/translate/search">Search</a></li><li><a href="/admin/build/translate/import">Import</a></li><li><a href="/admin/build/translate/export">Export</a></li></ul></li><li class="expandable"><a href="/admin/build/path">URL aliases</a> 
<ul><li><a href="/admin/build/path/list">List</a></li><li><a href="/admin/build/path/add">Add alias</a></li><li><a href="/admin/build/path/pathauto">Automated alias settings</a></li><li><a href="/admin/build/path/delete_bulk">Delete aliases</a></li></ul></li><li class="expandable"><a href="/admin/build/path-redirect">URL redirects</a> 
<ul><li><a href="/admin/build/path-redirect/list">List</a></li><li><a href="/admin/build/path-redirect/add">Add redirect</a></li><li><a href="/admin/build/path-redirect/settings">Settings</a></li></ul></li><li class="expandable"><a href="/admin/build/views">Views</a> 
<ul><li><a href="/admin/build/views/list">List</a></li><li><a href="/admin/build/views/add">Add</a></li><li><a href="/admin/build/views/import">Import</a></li><li class="expandable"><a href="/admin/build/views/tools">Tools</a> 
<ul><li><a href="/admin/build/views/tools/basic">Basic</a></li><li><a href="/admin/build/views/tools/export">Bulk export</a></li><li><a href="/admin/build/views/tools/convert">Convert</a></li></ul></li></ul></li></ul></li><li class="expandable"><a href="/admin/settings">Site configuration</a> 
<ul><li><a href="/admin/by-module">By module</a></li><li class="expandable"><a href="/admin/settings/actions">Actions</a> 
<ul><li><a href="/admin/settings/actions/manage">Manage actions</a></li></ul></li><li><a href="/admin/settings/admin_menu">Administration menu</a></li><li class="expandable"><a href="/admin/settings/admin">Administration tools</a> 
<ul><li><a href="/admin/settings/admin/settings">Settings</a></li><li><a href="/admin/settings/admin/theme">Administration theme</a></li><li><a href="/admin/settings/admin/rebuild">Rebuild</a></li></ul></li><li><a href="/admin/settings/advanced-profile">Advanced Profile Kit</a></li><li><a href="/admin/settings/ckeditor">CKEditor</a></li><li><a href="/admin/settings/clean-urls">Clean URLs</a></li><li class="expandable"><a href="/admin/settings/comment_notify">Comment notify</a> 
<ul><li><a href="/admin/settings/comment_notify/settings">Settings</a></li><li><a href="/admin/settings/comment_notify/unsubscribe">Unsubscribe</a></li></ul></li><li><a href="/admin/settings/date_popup">Date Popup Configuration</a></li><li class="expandable"><a href="/admin/settings/date-time">Date and time</a> 
<ul><li><a href="/admin/settings/date-time/configure">Date and time</a></li><li class="expandable"><a href="/admin/settings/date-time/formats">Formats</a> 
<ul><li><a href="/admin/settings/date-time/formats/configure">Configure</a></li><li><a href="/admin/settings/date-time/formats/custom">Custom formats</a></li><li><a href="/admin/settings/date-time/formats/add">Add format</a></li></ul></li></ul></li><li><a href="/admin/settings/devel">Devel settings</a></li><li><a href="/admin/settings/error-reporting">Error reporting</a></li><li><a href="/admin/settings/extlink">External links</a></li><li><a href="/admin/settings/feedapi">FeedAPI</a></li><li><a href="/admin/settings/file-system">File system</a></li><li><a href="/admin/settings/gmap">GMap</a></li><li><a href="/admin/settings/globalredirect">Global Redirect</a></li><li><a href="/admin/settings/googleanalytics">Google Analytics</a></li><li><a href="/admin/settings/imce">IMCE</a></li><li><a href="/admin/settings/image-toolkit">Image toolkit</a></li><li class="expandable"><a href="/admin/settings/imageapi">ImageAPI</a> 
<ul><li><a href="/admin/settings/imageapi/list">List</a></li><li class="expandable"><a href="/admin/settings/imageapi/config">Configure</a> 
<ul><li><a href="/admin/settings/imageapi/config/imageapi_gd">ImageAPI GD2</a></li></ul></li></ul></li><li class="expandable"><a href="/admin/settings/filters">Input formats</a> 
<ul><li><a href="/admin/settings/filters/list">List</a></li><li><a href="/admin/settings/filters/add">Add input format</a></li></ul></li><li class="expandable"><a href="/admin/settings/language">Languages</a> 
<ul><li><a href="/admin/settings/language/overview">List</a></li><li><a href="/admin/settings/language/add">Add language</a></li><li><a href="/admin/settings/language/configure">Configure</a></li></ul></li><li class="expandable"><a href="/admin/settings/location">Location</a> 
<ul><li><a href="/admin/settings/location/main">Main settings</a></li><li><a href="/admin/settings/location/maplinking">Map links</a></li><li><a href="/admin/settings/location/geocoding">Geocoding options</a></li><li><a href="/admin/settings/location/util">Location utilities</a></li></ul></li><li class="expandable"><a href="/admin/settings/logging">Logging and alerts</a> 
<ul><li><a href="/admin/settings/logging/dblog">Database logging</a></li><li><a href="/admin/settings/logging/syslog">Syslog</a></li></ul></li><li><a href="/admin/settings/media_youtube">Media: YouTube</a></li><li><a href="/admin/settings/menu_breadcrumb">Menu breadcrumb</a></li><li><a href="/admin/settings/nws_weather">NWS Weather</a></li><li><a href="/admin/settings/openidurl">OpenID URL</a></li><li><a href="/admin/settings/pngfix">PNG Fix</a></li><li><a href="/admin/settings/performance">Performance</a></li><li class="expandable"><a href="/admin/settings/performance_logging">Performance logging</a> 
<ul><li><a href="/admin/settings/performance_logging/apc_clear">Clear APC</a></li><li><a href="/admin/settings/performance_logging/memcache_clear">Clear Memcache</a></li></ul></li><li class="expandable"><a href="/admin/settings/rpx">RPX</a> 
<ul><li><a href="/admin/settings/rpx/settings">Settings</a></li><li><a href="/admin/settings/rpx/profile">Profile Fields</a></li></ul></li><li><a href="/admin/settings/search">Search settings</a></li><li class="expandable"><a href="/admin/settings/sharedblocks">Shared Blocks</a> 
<ul><li><a href="/admin/settings/sharedblocks/publish">Published Blocks</a></li><li class="expandable"><a href="/admin/settings/sharedblocks/subscribe">Subscribe Blocks</a> 
<ul><li><a href="/admin/settings/sharedblocks/subscribe/add">Add a Block Subscription</a></li></ul></li></ul></li><li><a href="/admin/settings/site-information">Site information</a></li><li><a href="/admin/settings/site-maintenance">Site maintenance</a></li><li><a href="/admin/settings/tagadelic">Tagadelic configuration</a></li><li><a href="/admin/settings/taxonomy_manager">Taxonomy Manager</a></li><li><a href="/admin/settings/throttle">Throttle</a></li><li><a href="/admin/settings/tree_menus">Tree Menus</a></li><li><a href="/admin/settings/weather">Weather</a></li><li><a href="/admin/settings/webform">Webform settings</a></li><li class="expandable"><a href="/admin/settings/xmlsitemap">XML sitemap</a> 
<ul><li><a href="/admin/settings/xmlsitemap/list">List</a></li><li class="expandable"><a href="/admin/settings/xmlsitemap/custom">Custom links</a> 
<ul><li><a href="/admin/settings/xmlsitemap/custom/list">List</a></li><li><a href="/admin/settings/xmlsitemap/custom/add">Add custom link</a></li></ul></li><li><a href="/admin/settings/xmlsitemap/engines">Search Engines</a></li><li><a href="/admin/settings/xmlsitemap/settings">Settings</a></li><li><a href="/admin/settings/xmlsitemap/rebuild">Rebuild links</a></li></ul></li><li><a href="/admin/settings/getid3">getID3()</a></li><li><a href="/admin/settings/jquery_update">jQuery Update</a></li></ul></li><li class="expandable"><a href="/admin/messaging">Messaging &amp; Notifications</a> 
<ul><li class="expandable"><a href="/admin/messaging/subscriptions">Manage subscriptions</a> 
<ul><li><a href="/admin/messaging/subscriptions/overview">Overview</a></li><li><a href="/admin/messaging/subscriptions/admin">Administer</a></li><li><a href="/admin/messaging/subscriptions/queue">Queue</a></li></ul></li><li><a href="/admin/messaging/template">Message templates</a></li><li class="expandable"><a href="/admin/messaging/settings">Messaging settings</a> 
<ul><li><a href="/admin/messaging/settings/overview">Messaging</a></li><li class="expandable"><a href="/admin/messaging/settings/method">Send methods</a> 
<ul><li><a href="/admin/messaging/settings/method/overview">General</a></li><li><a href="/admin/messaging/settings/method/phpmailer">PHPMailer</a></li></ul></li><li><a href="/admin/messaging/settings/test">Test</a></li></ul></li><li class="expandable"><a href="/admin/messaging/notifications">Notifications Settings</a> 
<ul><li><a href="/admin/messaging/notifications/settings">General</a></li><li><a href="/admin/messaging/notifications/content">Content subscriptions</a></li><li><a href="/admin/messaging/notifications/events">Events</a></li><li><a href="/admin/messaging/notifications/intervals">Intervals</a></li><li><a href="/admin/messaging/notifications/ui">User interface</a></li></ul></li></ul></li><li class="expandable"><a href="/admin/user">User management</a> 
<ul><li class="expandable"><a href="/admin/user/rules">Access rules</a> 
<ul><li><a href="/admin/user/rules/list">List</a></li><li><a href="/admin/user/rules/add">Add rule</a></li><li><a href="/admin/user/rules/check">Check rules</a></li></ul></li><li class="expandable"><a href="/admin/user/captcha">CAPTCHA</a> 
<ul><li class="expandable"><a href="/admin/user/captcha/captcha">CAPTCHA</a> 
<ul><li><a href="/admin/user/captcha/captcha/settings">General settings</a></li><li><a href="/admin/user/captcha/captcha/examples">Examples</a></li></ul></li><li><a href="/admin/user/captcha/recaptcha">reCAPTCHA</a></li></ul></li><li><a href="/admin/user/gravatar">Gravatar</a></li><li><a href="/admin/user/htpasswdsync">HTPassword Sync</a></li><li><a href="/admin/user/permissions">Permissions</a></li><li><a href="/admin/user/profile_location">Profile location settings</a></li><li><a href="/admin/user/profile">Profiles</a></li><li><a href="/admin/user/profile_blog_info">Profiles Blog Information</a></li><li class="expandable"><a href="/admin/user/realname">RealName</a> 
<ul><li><a href="/admin/user/realname/general">General</a></li><li><a href="/admin/user/realname/fields">Fields</a></li><li><a href="/admin/user/realname/module">Module</a></li><li><a href="/admin/user/realname/bypass">Bypass Forms</a></li><li><a href="/admin/user/realname/recalc">Recalculate names</a></li></ul></li><li><a href="/admin/user/roles">Roles</a></li><li><a href="/admin/user/settings">User settings</a></li><li class="expandable"><a href="/admin/user/user">Users</a> 
<ul><li><a href="/admin/user/user/list">List</a></li><li><a href="/admin/user/user/create">Add user</a></li></ul></li></ul></li><li class="expandable"><a href="/admin/reports">Reports</a> 
<ul><li><a href="/admin/reports/dblog">Recent log entries</a></li><li><a href="/admin/reports/performance_logging_details">Performance Logs: Details</a></li><li><a href="/admin/reports/performance_logging_summary">Performance Logs: Summary</a></li><li><a href="/admin/reports/access-denied">Top &#039;access denied&#039; errors</a></li><li><a href="/admin/reports/page-not-found">Top &#039;page not found&#039; errors</a></li><li><a href="/admin/reports/search">Top search phrases</a></li><li class="expandable"><a href="/admin/reports/updates">Available updates</a> 
<ul><li><a href="/admin/reports/updates/list">List</a></li><li><a href="/admin/reports/updates/settings">Settings</a></li></ul></li><li><a href="/admin/reports/status">Status report</a></li></ul></li><li><a href="/admin/advanced_help">Advanced help</a></li><li><a href="/admin/help">Help</a></li></ul></div> 
<div id='admin-toolbar' class='nw vertical df'> 
  <span class='admin-toggle'>Admin</span> 
 
  <div class='admin-blocks admin-blocks-4'> 
    <div class='admin-tabs clear-block'> 
              <div class='admin-tab admin-create admin-tab-active'><span  id="admin-tab-admin-create">Create content</span></div>              <div class='admin-tab admin-theme'><span  id="admin-tab-admin-theme">Switch theme</span></div>              <div class='admin-tab admin-account'><span  id="admin-tab-admin-account">My Account</span></div>              <div class='admin-tab admin-menu'><span  id="admin-tab-admin-menu">Administer</span></div>          </div> 
 
          <div class='admin-block admin-active' id='block-admin-create'> 
        <div class='block-content clear-block'><ul class="menu"><li class="leaf first"><a href="/node/add/2col">2 columns</a></li> 
<li class="leaf"><a href="/node/add/2col-w-nav">2 columns with navigation</a></li> 
<li class="leaf"><a href="/node/add/3-columns">3 columns</a></li> 
<li class="leaf"><a href="/node/add/activities">Activities</a></li> 
<li class="leaf"><a href="/node/add/citycounty">City &amp; County Info</a></li> 
<li class="leaf"><a href="/node/add/communityvision">Community Vision</a></li> 
<li class="leaf"><a href="/node/add/event" title="Events have a start date and an optional end date as well as a teaser and a body. They can be extended by other modules, too.">Event<span class='menu-description'>Events have a start date and an optional end date as well as a teaser and a body. They can be extended by other modules, too.</span></a></li> 
<li class="leaf"><a href="/node/add/fooddining">Food &amp; Dining</a></li> 
<li class="leaf"><a href="/node/add/forum" title="A forum topic is the initial post to a new discussion thread within a forum.">Forum topic<span class='menu-description'>A forum topic is the initial post to a new discussion thread within a forum.</span></a></li> 
<li class="leaf"><a href="/node/add/itinerary-ideas">Itinerary Ideas</a></li> 
<li class="leaf"><a href="/node/add/lodging">Lodging</a></li> 
<li class="leaf"><a href="/node/add/lodgingdeal">Lodging deal</a></li> 
<li class="leaf"><a href="/node/add/news">News</a></li> 
<li class="leaf"><a href="/node/add/organization" title="This is a group page content type for organizations, agencies and departments.">Organization<span class='menu-description'>This is a group page content type for organizations, agencies and departments.</span></a></li> 
<li class="leaf"><a href="/node/add/org-announce" title="Content-type for postings by organizations, agencies and departments.">Organization Announcement<span class='menu-description'>Content-type for postings by organizations, agencies and departments.</span></a></li> 
<li class="leaf"><a href="/node/add/outdoors">Outdoors</a></li> 
<li class="leaf"><a href="/node/add/page" title="A page, similar in form to a story, is a simple method for creating and displaying information that rarely changes, such as an &quot;About us&quot; section of a website. By default, a page entry does not allow visitor comments and is not featured on the site&#039;s initial home page.">Page<span class='menu-description'>A page, similar in form to a story, is a simple method for creating and displaying information that rarely changes, such as an &quot;About us&quot; section ...</span></a></li> 
<li class="leaf"><a href="/node/add/poll" title="A poll is a question with a set of possible responses. A poll, once created, automatically provides a simple running count of the number of votes received for each response.">Poll<span class='menu-description'>A poll is a question with a set of possible responses. A poll, once created, automatically provides a simple running count of the number of votes ...</span></a></li> 
<li class="leaf"><a href="/node/add/profile" title="A user profile built as content.">Profile<span class='menu-description'>A user profile built as content.</span></a></li> 
<li class="leaf"><a href="/node/add/story" title="A story, similar in form to a page, is ideal for creating and displaying content that informs or engages website visitors. Press releases, site announcements, and informal blog-like entries may all be created with a story entry. By default, a story entry is automatically featured on the site&#039;s initial home page, and provides the ability to post comments.">Story<span class='menu-description'>A story, similar in form to a page, is ideal for creating and displaying content that informs or engages website visitors. Press releases, site ...</span></a></li> 
<li class="leaf"><a href="/node/add/webform" title="Create a new form or questionnaire accessible to users. Submission results and statistics are recorded and accessible to privileged users.">Webform<span class='menu-description'>Create a new form or questionnaire accessible to users. Submission results and statistics are recorded and accessible to privileged users.</span></a></li> 
<li class="leaf last"><a href="/node/add/whatsnew">What</a></li> 
</ul></div> 
      </div> 
          <div class='admin-block ' id='block-admin-theme'> 
        <div class='block-content clear-block'><form action="/admin/build/block"  accept-charset="UTF-8" method="post" id="admin-block-theme-form"> 
<div><div class="form-radios"></div><input type="submit" name="op" id="edit-submit-1" value="Save configuration"  class="form-submit" /> 
<input type="hidden" name="form_build_id" id="form-4e83e6676be85e1bb2265753c8fa8992" value="form-4e83e6676be85e1bb2265753c8fa8992"  /> 
<input type="hidden" name="form_token" id="edit-admin-block-theme-form-form-token" value="ddac89e6efb74ff49f6f752872df5a39"  /> 
<input type="hidden" name="form_id" id="edit-admin-block-theme-form" value="admin_block_theme_form"  /> 
 
</div></form> 
</div> 
      </div> 
          <div class='admin-block ' id='block-admin-account'> 
        <div class='block-content clear-block'><div class="item-list"><ul class="menu"><li class="leaf first"><a href="/users/webadmin">View my account</a></li> 
<li class="leaf"><a href="/user/1/edit">Edit my account</a></li> 
<li class="leaf last"><a href="/logout">Logout</a></li> 
</ul></div></div> 
      </div> 
          <div class='admin-block ' id='block-admin-menu'> 
        <div class='block-content clear-block'><ul class="menu"><li class="expanded last"><a href="/admin">Administer</a><ul class="menu"><li class="expanded first"><a href="/admin/content" title="Manage your site&#039;s content.">Content management<span class='menu-description'>Manage your site&#039;s content.</span></a><ul class="menu"><li class="leaf first"><a href="/admin/content/backup_migrate" title="Backup/restore your database or migrate data to or from another Drupal site.">Backup and Migrate<span class='menu-description'>Backup/restore your database or migrate data to or from another Drupal site.</span></a></li> 
<li class="leaf"><a href="/admin/content/comment" title="List and edit site comments and the comment moderation queue.">Comments<span class='menu-description'>List and edit site comments and the comment moderation queue.</span></a></li> 
<li class="leaf"><a href="/admin/content/node" title="View, edit, and delete your site&#039;s content.">Content<span class='menu-description'>View, edit, and delete your site&#039;s content.</span></a></li> 
<li class="leaf"><a href="/admin/content/types" title="Manage posts by content type, including default status, front page promotion, etc.">Content types<span class='menu-description'>Manage posts by content type, including default status, front page promotion, etc.</span></a></li> 
<li class="leaf"><a href="/admin/content/date/tools" title="Tools to import and auto-create dates and calendars.">Date Tools<span class='menu-description'>Tools to import and auto-create dates and calendars.</span></a></li> 
<li class="leaf"><a href="/admin/content/emfield" title="Configure Embedded Media Field: Allow content types to use various 3rd party providers, enter API keys, etc.">Embedded Media Field configuration<span class='menu-description'>Configure Embedded Media Field: Allow content types to use various 3rd party providers, enter API keys, etc.</span></a></li> 
<li class="leaf"><a href="/admin/content/aggregator" title="Configure which content your site aggregates from other sites, how often it polls them, and how they&#039;re categorized.">Feed aggregator<span class='menu-description'>Configure which content your site aggregates from other sites, how often it polls them, and how they&#039;re categorized.</span></a></li> 
<li class="expanded"><a href="/admin/content/feed" title="Overview which content your site aggregates from other sites and see detailed statistics about the feeds.">Feeds<span class='menu-description'>Overview which content your site aggregates from other sites and see detailed statistics about the feeds.</span></a><ul class="menu"><li class="leaf first"><a href="/admin/content/feed/export_opml">Export all feeds as OPML</a></li> 
<li class="leaf last"><a href="/admin/content/feed/import_opml">Import OPML</a></li> 
</ul></li> 
<li class="leaf"><a href="/admin/content/forum" title="Control forums and their hierarchy and change forum settings.">Forums<span class='menu-description'>Control forums and their hierarchy and change forum settings.</span></a></li> 
<li class="leaf"><a href="/admin/content/nodewords" title="Configure HTML meta tags for all the content.">Meta tags<span class='menu-description'>Configure HTML meta tags for all the content.</span></a></li> 
<li class="leaf"><a href="/admin/content/page_title" title="Enhanced control over the page titles (in the &amp;lt;head&amp;gt; tag).">Page titles<span class='menu-description'>Enhanced control over the page titles (in the &amp;lt;head&amp;gt; tag).</span></a></li> 
<li class="leaf"><a href="/admin/content/node-settings" title="Control posting behavior, such as teaser length, requiring previews before posting, and the number of posts on the front page.">Post settings<span class='menu-description'>Control posting behavior, such as teaser length, requiring previews before posting, and the number of posts on the front page.</span></a></li> 
<li class="leaf"><a href="/admin/content/rss-publishing" title="Configure the number of items per feed and whether feeds should be titles/teasers/full-text.">RSS publishing<span class='menu-description'>Configure the number of items per feed and whether feeds should be titles/teasers/full-text.</span></a></li> 
<li class="leaf"><a href="/admin/content/taxonomy" title="Manage tagging, categorization, and classification of your content.">Taxonomy<span class='menu-description'>Manage tagging, categorization, and classification of your content.</span></a></li> 
<li class="leaf"><a href="/admin/content/taxonomy_manager" title="Administer vocabularies with the Taxonomy Manager">Taxonomy Manager<span class='menu-description'>Administer vocabularies with the Taxonomy Manager</span></a></li> 
<li class="leaf last"><a href="/admin/content/webform" title="View and edit all the available webforms on your site.">Webforms<span class='menu-description'>View and edit all the available webforms on your site.</span></a></li> 
</ul></li> 
<li class="expanded"><a href="/admin/build" title="Control how your site looks and feels.">Site building<span class='menu-description'>Control how your site looks and feels.</span></a><ul class="menu"><li class="leaf first"><a href="/admin/build/block" title="Configure what block content appears in your site&#039;s sidebars and other regions." class="active">Blocks<span class='menu-description'>Configure what block content appears in your site&#039;s sidebars and other regions.</span></a></li> 
<li class="leaf"><a href="/admin/build/contact" title="Create a system contact form and set up categories for the form to use.">Contact form<span class='menu-description'>Create a system contact form and set up categories for the form to use.</span></a></li> 
<li class="leaf"><a href="/admin/build/domain" title="Settings for the Domain Access module.">Domains<span class='menu-description'>Settings for the Domain Access module.</span></a></li> 
<li class="leaf"><a href="/admin/build/imagecache" title="Administer imagecache presets and actions.">ImageCache<span class='menu-description'>Administer imagecache presets and actions.</span></a></li> 
<li class="leaf"><a href="/admin/build/menu" title="Control your site&#039;s navigation menu, primary links and secondary links. as well as rename and reorganize menu items.">Menus<span class='menu-description'>Control your site&#039;s navigation menu, primary links and secondary links. as well as rename and reorganize menu items.</span></a></li> 
<li class="leaf"><a href="/admin/build/modules" title="Enable or disable add-on modules for your site.">Modules<span class='menu-description'>Enable or disable add-on modules for your site.</span></a></li> 
<li class="leaf"><a href="/admin/build/themes" title="Change which theme your site uses or allows users to set.">Themes<span class='menu-description'>Change which theme your site uses or allows users to set.</span></a></li> 
<li class="leaf"><a href="/admin/build/translate" title="Translate the built in interface and optionally other text.">Translate interface<span class='menu-description'>Translate the built in interface and optionally other text.</span></a></li> 
<li class="leaf"><a href="/admin/build/path" title="Change your site&#039;s URL paths by aliasing them.">URL aliases<span class='menu-description'>Change your site&#039;s URL paths by aliasing them.</span></a></li> 
<li class="leaf"><a href="/admin/build/path-redirect" title="Redirect users from one URL to another.">URL redirects<span class='menu-description'>Redirect users from one URL to another.</span></a></li> 
<li class="leaf last"><a href="/admin/build/views" title="Views are customized lists of content on your system; they are highly configurable and give you control over how lists of content are presented.">Views<span class='menu-description'>Views are customized lists of content on your system; they are highly configurable and give you control over how lists of content are presented.</span></a></li> 
</ul></li> 
<li class="expanded"><a href="/admin/settings" title="Adjust basic site configuration options.">Site configuration<span class='menu-description'>Adjust basic site configuration options.</span></a><ul class="menu"><li class="leaf first"><a href="/admin/settings/actions" title="Manage the actions defined for your site.">Actions<span class='menu-description'>Manage the actions defined for your site.</span></a></li> 
<li class="leaf"><a href="/admin/settings/admin_menu" title="Adjust administration menu settings.">Administration menu<span class='menu-description'>Adjust administration menu settings.</span></a></li> 
<li class="leaf"><a href="/admin/settings/admin" title="Settings for site administration tools.">Administration tools<span class='menu-description'>Settings for site administration tools.</span></a></li> 
<li class="leaf"><a href="/admin/settings/advanced-profile">Advanced Profile Kit</a></li> 
<li class="leaf"><a href="/admin/settings/ckeditor" title="Configure the rich text editor.">CKEditor<span class='menu-description'>Configure the rich text editor.</span></a></li> 
<li class="leaf"><a href="/admin/settings/clean-urls" title="Enable or disable clean URLs for your site.">Clean URLs<span class='menu-description'>Enable or disable clean URLs for your site.</span></a></li> 
<li class="leaf"><a href="/admin/settings/comment_notify" title="Configure settings for e-mails about new comments.">Comment notify<span class='menu-description'>Configure settings for e-mails about new comments.</span></a></li> 
<li class="leaf"><a href="/admin/settings/date_popup" title="Allows the user to configure the Date Popup settings.">Date Popup Configuration<span class='menu-description'>Allows the user to configure the Date Popup settings.</span></a></li> 
<li class="leaf"><a href="/admin/settings/date-time" title="Settings for how Drupal displays date and time, as well as the system&#039;s default timezone.">Date and time<span class='menu-description'>Settings for how Drupal displays date and time, as well as the system&#039;s default timezone.</span></a></li> 
<li class="leaf"><a href="/admin/settings/devel" title="Helper functions, pages, and blocks to assist Drupal developers. The devel blocks can be managed via the block administration page.">Devel settings<span class='menu-description'>Helper functions, pages, and blocks to assist Drupal developers. The devel blocks can be managed via the block administration page.</span></a></li> 
<li class="leaf"><a href="/admin/settings/error-reporting" title="Control how Drupal deals with errors including 403/404 errors as well as PHP error reporting.">Error reporting<span class='menu-description'>Control how Drupal deals with errors including 403/404 errors as well as PHP error reporting.</span></a></li> 
<li class="leaf"><a href="/admin/settings/extlink" title="Alter the display of external links on the site.">External links<span class='menu-description'>Alter the display of external links on the site.</span></a></li> 
<li class="leaf"><a href="/admin/settings/feedapi" title="Configure advanced options for FeedAPI module.">FeedAPI<span class='menu-description'>Configure advanced options for FeedAPI module.</span></a></li> 
<li class="leaf"><a href="/admin/settings/file-system" title="Tell Drupal where to store uploaded files and how they are accessed.">File system<span class='menu-description'>Tell Drupal where to store uploaded files and how they are accessed.</span></a></li> 
<li class="leaf"><a href="/admin/settings/gmap" title="Configure GMap settings">GMap<span class='menu-description'>Configure GMap settings</span></a></li> 
<li class="leaf"><a href="/admin/settings/globalredirect" title="Chose which features you would like enabled for Global Redirect">Global Redirect<span class='menu-description'>Chose which features you would like enabled for Global Redirect</span></a></li> 
<li class="leaf"><a href="/admin/settings/googleanalytics" title="Configure the settings used to generate your Google Analytics tracking code.">Google Analytics<span class='menu-description'>Configure the settings used to generate your Google Analytics tracking code.</span></a></li> 
<li class="leaf"><a href="/admin/settings/imce" title="Control how your image/file browser works.">IMCE<span class='menu-description'>Control how your image/file browser works.</span></a></li> 
<li class="leaf"><a href="/admin/settings/image-toolkit" title="Choose which image toolkit to use if you have installed optional toolkits.">Image toolkit<span class='menu-description'>Choose which image toolkit to use if you have installed optional toolkits.</span></a></li> 
<li class="leaf"><a href="/admin/settings/imageapi" title="Configure ImageAPI.">ImageAPI<span class='menu-description'>Configure ImageAPI.</span></a></li> 
<li class="leaf"><a href="/admin/settings/filters" title="Configure how content input by users is filtered, including allowed HTML tags. Also allows enabling of module-provided filters.">Input formats<span class='menu-description'>Configure how content input by users is filtered, including allowed HTML tags. Also allows enabling of module-provided filters.</span></a></li> 
<li class="leaf"><a href="/admin/settings/language" title="Configure languages for content and the user interface.">Languages<span class='menu-description'>Configure languages for content and the user interface.</span></a></li> 
<li class="leaf"><a href="/admin/settings/location" title="Settings for Location module">Location<span class='menu-description'>Settings for Location module</span></a></li> 
<li class="expanded"><a href="/admin/settings/logging" title="Settings for logging and alerts modules. Various modules can route Drupal&#039;s system events to different destination, such as syslog, database, email, ...etc.">Logging and alerts<span class='menu-description'>Settings for logging and alerts modules. Various modules can route Drupal&#039;s system events to different destination, such as syslog, database, ...</span></a><ul class="menu"><li class="leaf first"><a href="/admin/settings/logging/dblog" title="Settings for logging to the Drupal database logs. This is the most common method for small to medium sites on shared hosting. The logs are viewable from the admin pages.">Database logging<span class='menu-description'>Settings for logging to the Drupal database logs. This is the most common method for small to medium sites on shared hosting. The logs are ...</span></a></li> 
<li class="leaf last"><a href="/admin/settings/logging/syslog" title="Settings for syslog logging. Syslog is an operating system administrative logging tool used in systems management and security auditing. Most suited to medium and large sites, syslog provides filtering tools that allow messages to be routed by type and severity.">Syslog<span class='menu-description'>Settings for syslog logging. Syslog is an operating system administrative logging tool used in systems management and security auditing. Most ...</span></a></li> 
</ul></li> 
<li class="leaf"><a href="/admin/settings/media_youtube" title="Administer the Media: YouTube module.">Media: YouTube<span class='menu-description'>Administer the Media: YouTube module.</span></a></li> 
<li class="leaf"><a href="/admin/settings/menu_breadcrumb" title="Configure menu breadcrumb.">Menu breadcrumb<span class='menu-description'>Configure menu breadcrumb.</span></a></li> 
<li class="leaf"><a href="/admin/settings/nws_weather" title="NWS weather retrieves weather data from the National Weather Service via a SOAP interface.">NWS Weather<span class='menu-description'>NWS weather retrieves weather data from the National Weather Service via a SOAP interface.</span></a></li> 
<li class="leaf"><a href="/admin/settings/openidurl" title="Delegate your URL as an OpenID.">OpenID URL<span class='menu-description'>Delegate your URL as an OpenID.</span></a></li> 
<li class="leaf"><a href="/admin/settings/pngfix" title="Configure how the PNG Fix operates.">PNG Fix<span class='menu-description'>Configure how the PNG Fix operates.</span></a></li> 
<li class="leaf"><a href="/admin/settings/performance" title="Enable or disable page caching for anonymous users and set CSS and JS bandwidth optimization options.">Performance<span class='menu-description'>Enable or disable page caching for anonymous users and set CSS and JS bandwidth optimization options.</span></a></li> 
<li class="expanded"><a href="/admin/settings/performance_logging" title="Logs performance data: page generation times and memory usage.">Performance logging<span class='menu-description'>Logs performance data: page generation times and memory usage.</span></a><ul class="menu"><li class="leaf first"><a href="/admin/settings/performance_logging/apc_clear" title="Clears performance statistics collected in APC.">Clear APC<span class='menu-description'>Clears performance statistics collected in APC.</span></a></li> 
<li class="leaf last"><a href="/admin/settings/performance_logging/memcache_clear" title="Clears performance statistics collected in Memcache.">Clear Memcache<span class='menu-description'>Clears performance statistics collected in Memcache.</span></a></li> 
</ul></li> 
<li class="leaf"><a href="/admin/settings/rpx" title="Configure the settings for RPX.">RPX<span class='menu-description'>Configure the settings for RPX.</span></a></li> 
<li class="leaf"><a href="/admin/settings/search" title="Configure relevance settings for search and other indexing options">Search settings<span class='menu-description'>Configure relevance settings for search and other indexing options</span></a></li> 
<li class="leaf"><a href="/admin/settings/sharedblocks" title="Configuration of shared blocks.">Shared Blocks<span class='menu-description'>Configuration of shared blocks.</span></a></li> 
<li class="leaf"><a href="/admin/settings/site-information" title="Change basic site information, such as the site name, slogan, e-mail address, mission, front page and more.">Site information<span class='menu-description'>Change basic site information, such as the site name, slogan, e-mail address, mission, front page and more.</span></a></li> 
<li class="leaf"><a href="/admin/settings/site-maintenance" title="Take the site off-line for maintenance or bring it back online.">Site maintenance<span class='menu-description'>Take the site off-line for maintenance or bring it back online.</span></a></li> 
<li class="leaf"><a href="/admin/settings/tagadelic" title="Configure the tag clouds. Set the order, the number of tags, and the depth of the clouds.">Tagadelic configuration<span class='menu-description'>Configure the tag clouds. Set the order, the number of tags, and the depth of the clouds.</span></a></li> 
<li class="leaf"><a href="/admin/settings/taxonomy_manager" title="Advanced settings for the Taxonomy Manager">Taxonomy Manager<span class='menu-description'>Advanced settings for the Taxonomy Manager</span></a></li> 
<li class="leaf"><a href="/admin/settings/throttle" title="Control how your site cuts out content during heavy load.">Throttle<span class='menu-description'>Control how your site cuts out content during heavy load.</span></a></li> 
<li class="leaf"><a href="/admin/settings/tree_menus" title="Configure Tree Menus">Tree Menus<span class='menu-description'>Configure Tree Menus</span></a></li> 
<li class="leaf"><a href="/admin/settings/weather" title="Configure system-wide weather blocks and the default configuration for new locations.">Weather<span class='menu-description'>Configure system-wide weather blocks and the default configuration for new locations.</span></a></li> 
<li class="leaf"><a href="/admin/settings/webform" title="Global configuration of webform functionality.">Webform settings<span class='menu-description'>Global configuration of webform functionality.</span></a></li> 
<li class="leaf"><a href="/admin/settings/xmlsitemap" title="Configure the XML sitemaps.">XML sitemap<span class='menu-description'>Configure the XML sitemaps.</span></a></li> 
<li class="leaf"><a href="/admin/settings/getid3" title="Configure settings associated with getID3().">getID3()<span class='menu-description'>Configure settings associated with getID3().</span></a></li> 
<li class="leaf last"><a href="/admin/settings/jquery_update" title="Configure settings for jQuery Update module.">jQuery Update<span class='menu-description'>Configure settings for jQuery Update module.</span></a></li> 
</ul></li> 
<li class="expanded"><a href="/admin/messaging" title="Administer and configure messaging and notifications">Messaging &amp; Notifications<span class='menu-description'>Administer and configure messaging and notifications</span></a><ul class="menu"><li class="leaf first"><a href="/admin/messaging/subscriptions" title="Manage existing subscriptions and queue.">Manage subscriptions<span class='menu-description'>Manage existing subscriptions and queue.</span></a></li> 
<li class="leaf"><a href="/admin/messaging/template" title="Configuration of message templates">Message templates<span class='menu-description'>Configuration of message templates</span></a></li> 
<li class="leaf"><a href="/admin/messaging/settings" title="Configuration of messaging framework">Messaging settings<span class='menu-description'>Configuration of messaging framework</span></a></li> 
<li class="leaf last"><a href="/admin/messaging/notifications" title="Site settings for user notifications.">Notifications Settings<span class='menu-description'>Site settings for user notifications.</span></a></li> 
</ul></li> 
<li class="expanded"><a href="/admin/user" title="Manage your site&#039;s users, groups and access to site features.">User management<span class='menu-description'>Manage your site&#039;s users, groups and access to site features.</span></a><ul class="menu"><li class="leaf first"><a href="/admin/user/rules" title="List and create rules to disallow usernames, e-mail addresses, and IP addresses.">Access rules<span class='menu-description'>List and create rules to disallow usernames, e-mail addresses, and IP addresses.</span></a></li> 
<li class="leaf"><a href="/admin/user/captcha" title="Administer how and where CAPTCHAs are used.">CAPTCHA<span class='menu-description'>Administer how and where CAPTCHAs are used.</span></a></li> 
<li class="leaf"><a href="/admin/user/gravatar" title="Administer Gravatar integration.">Gravatar<span class='menu-description'>Administer Gravatar integration.</span></a></li> 
<li class="leaf"><a href="/admin/user/htpasswdsync" title="Preferences for the HTPasswd Sync module">HTPassword Sync<span class='menu-description'>Preferences for the HTPasswd Sync module</span></a></li> 
<li class="leaf"><a href="/admin/user/permissions" title="Determine access to features by selecting permissions for roles.">Permissions<span class='menu-description'>Determine access to features by selecting permissions for roles.</span></a></li> 
<li class="leaf"><a href="/admin/user/profile_location" title="Configure your profile location preferences and field assignments.">Profile location settings<span class='menu-description'>Configure your profile location preferences and field assignments.</span></a></li> 
<li class="leaf"><a href="/admin/user/profile" title="Create customizable fields for your users.">Profiles<span class='menu-description'>Create customizable fields for your users.</span></a></li> 
<li class="leaf"><a href="/admin/user/profile_blog_info" title="Add a blog fields to profiles.">Profiles Blog Information<span class='menu-description'>Add a blog fields to profiles.</span></a></li> 
<li class="leaf"><a href="/admin/user/realname" title="Configure which fields are used to create a user&#039;s RealName.">RealName<span class='menu-description'>Configure which fields are used to create a user&#039;s RealName.</span></a></li> 
<li class="leaf"><a href="/admin/user/roles" title="List, edit, or add user roles.">Roles<span class='menu-description'>List, edit, or add user roles.</span></a></li> 
<li class="leaf"><a href="/admin/user/settings" title="Configure default behavior of users, including registration requirements, e-mails, and user pictures.">User settings<span class='menu-description'>Configure default behavior of users, including registration requirements, e-mails, and user pictures.</span></a></li> 
<li class="leaf last"><a href="/admin/user/user" title="List, add, and edit users.">Users<span class='menu-description'>List, add, and edit users.</span></a></li> 
</ul></li> 
<li class="expanded"><a href="/admin/reports" title="View reports from system logs and other status information.">Reports<span class='menu-description'>View reports from system logs and other status information.</span></a><ul class="menu"><li class="leaf first"><a href="/admin/reports/dblog" title="View events that have recently been logged.">Recent log entries<span class='menu-description'>View events that have recently been logged.</span></a></li> 
<li class="leaf"><a href="/admin/reports/performance_logging_details" title="View detailed, per page, performance logs: page generation times and memory usage.">Performance Logs: Details<span class='menu-description'>View detailed, per page, performance logs: page generation times and memory usage.</span></a></li> 
<li class="leaf"><a href="/admin/reports/performance_logging_summary" title="View summary performance logs: page generation times and memory usage.">Performance Logs: Summary<span class='menu-description'>View summary performance logs: page generation times and memory usage.</span></a></li> 
<li class="leaf"><a href="/admin/reports/access-denied" title="View &#039;access denied&#039; errors (403s).">Top 'access denied' errors<span class='menu-description'>View &#039;access denied&#039; errors (403s).</span></a></li> 
<li class="leaf"><a href="/admin/reports/page-not-found" title="View &#039;page not found&#039; errors (404s).">Top 'page not found' errors<span class='menu-description'>View &#039;page not found&#039; errors (404s).</span></a></li> 
<li class="leaf"><a href="/admin/reports/search" title="View most popular search phrases.">Top search phrases<span class='menu-description'>View most popular search phrases.</span></a></li> 
<li class="leaf"><a href="/admin/reports/updates" title="Get a status report about available updates for your installed modules and themes.">Available updates<span class='menu-description'>Get a status report about available updates for your installed modules and themes.</span></a></li> 
<li class="leaf last"><a href="/admin/reports/status" title="Get a status report about your site&#039;s operation and any detected problems.">Status report<span class='menu-description'>Get a status report about your site&#039;s operation and any detected problems.</span></a></li> 
</ul></li> 
<li class="leaf"><a href="/admin/advanced_help">Advanced help</a></li> 
<li class="leaf last"><a href="/admin/help">Help</a></li> 
</ul></li> 
</ul></div> 
      </div> 
      </div> 
 
</div> 
<script type="text/javascript" src="/sites/all/modules/google_analytics/googleanalytics.js?I"></script> 
<script type="text/javascript"> 
<!--//--><![CDATA[//><!--
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
//--><!]]>
</script> 
<script type="text/javascript"> 
<!--//--><![CDATA[//><!--
try{var pageTracker = _gat._getTracker("UA-9959429-2");pageTracker._trackPageview();} catch(err) {}
//--><!]]>
</script> 
  </body>

</html>

