        <?php
                $event_date = current($node->field_event_date);
                $estart = $event_date['value'];
                $eend = $event_date['value2'];
                $estart = strtotime($estart);
                $eend = strtotime($eend);
                if($estart && $estart != $eend && isset($eend)) $eventtime = ' | '.date('ga',$estart). ' - '.date('ga',$eend);
                if(isset($estart) && !isset($eend)) $eventtime = ' | '.date('ga',$estart);
                $estart = date('F j, Y',$estart);
                $terms = taxonomy_node_get_terms($node);
                foreach( $terms as $eterm ) {
                    if( $eterm->vid == 3 ) $elocation = $eterm->name;
                }
        ?>
        <?php
		if($estart): ?><strong class="date"><span><?php print $estart; ?></span><?php if($eventtime) print $eventtime; ?></strong><?php endif; ?>
        <?php if($elocation): ?><em><?php print $elocation ?></em><?php endif; ?>
        <p><a href="<?php print $node_url; ?>"><?php print $title; ?></a></p>
