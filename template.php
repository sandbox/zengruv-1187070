<?php
// $Id: template.php,v 1.16.2.3 2010/05/11 09:41:22 goba Exp $

/**
 * Sets the body-tag class attribute.
 *
 * Adds 'sidebar-left', 'sidebar-right' or 'sidebars' classes as needed.
 */
function phptemplate_body_class($left, $right) {
  if ($left != '' && $right != '') {
    $class = 'sidebars';
  }
  else {
    if ($left != '') {
      $class = 'sidebar-left';
    }
    if ($right != '') {
      $class = 'sidebar-right';
    }
  }

  if (isset($class)) {
    print ' class="'. $class .'"';
  }
}

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return a string containing the breadcrumb output.
 */
function phptemplate_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
    return '<div class="breadcrumbs"><ul> <li>'. implode(' </li> <li> ', $breadcrumb) .' </li> </ul></div>';
  }
}

/**
 * Override or insert PHPTemplate variables into the templates.
 */
function phptemplate_preprocess_page(&$vars) {
  $vars['tabs2'] = menu_secondary_local_tasks();

  // Hook into color.module
  if (module_exists('color')) {
    _color_page_alter($vars);
  }

  if( ( arg(1) == 'add' &&  arg(2) == 'event' ) || ( (arg(2) == 'edit') && ($vars['node']->type == 'event') ) )
  {
    $vars['template_files'][] = 'page-add-event';
  }

  if (isset($vars['node']) && ($vars['node']->type == 'event' ) && arg(1) != 'add')
   {
       $vars['template_files'][] = 'page-event';
   }
   
  if (isset($vars['node']) && ($vars['node']->type == 'event' ) && arg(1) != 'add')
   {
       $vars['template_files'][] = 'page-event';
   }
}

/*  if ( $vars['node']->path == 'living' )
   {
       $vars['template_files'][] = 'page-living';
   }*/
function pueblocolorado_preprocess_page(&$variables) {
  $alias = drupal_get_path_alias($_GET['q']);
  if ($alias != $_GET['q']) {
   $template_filename = 'page';
    foreach (explode('/', $alias) as $path_part) {
     $template_filename = $template_filename . '-' . $path_part;
	 $variables['template_files'][] = $template_filename;
    }
  }
   
}

/**
 * Add a "Comments" heading above comments except on forum pages.
 */
function garland_preprocess_comment_wrapper(&$vars) {
  if ($vars['content'] && $vars['node']->type != 'forum') {
    $vars['content'] = '<h2 class="comments">'. t('Comments') .'</h2>'.  $vars['content'];
  }
}

/**
 * Returns the rendered local tasks. The default implementation renders
 * them as tabs. Overridden to split the secondary tasks.
 *
 * @ingroup themeable
 */
function phptemplate_menu_local_tasks() {
  return menu_primary_local_tasks();
}

/**
 * Returns the themed submitted-by string for the comment.
 */
function phptemplate_comment_submitted($comment) {
  return t('!datetime — !username',
    array(
      '!username' => theme('username', $comment),
      '!datetime' => format_date($comment->timestamp)
    ));
}

/**
 * Returns the themed submitted-by string for the node.
 */
function phptemplate_node_submitted($node) {
  return t('!datetime — !username',
    array(
      '!username' => theme('username', $node),
      '!datetime' => format_date($node->created),
    ));
}

/**
 * Generates IE CSS links for LTR and RTL languages.
 */
function phptemplate_get_ie7_styles() {
  $iecss = '<script type="text/javascript" src="'. base_path() . path_to_theme() .'/js/ie-png.js"></script>';
  return $iecss;
}
function phptemplate_get_ie8_styles() {
  $iecss = '<link type="text/css" rel="stylesheet" media="all" href="'. base_path() . path_to_theme() .'/css/ie.css" />';
  return $iecss;
}
function custom_get_terms_by_vocabulary($vid, $key = 'tid') {
  $result = db_query(db_rewrite_sql('SELECT t.tid, t.* FROM {term_data} t WHERE t.vid = %d ORDER BY weight', 't', 'tid'), $vid);
  $terms = array();
  while ($term = db_fetch_object($result)) {
    $terms[$term->$key] = $term;
  }
  return $terms;
}
function get_pueblo_events_count($startdate, $enddate, $event_category, $event_location) {
    $where = "";
    $flag = false;
    if($event_category && is_array($event_category))  {
          if(!in_array(0,$event_category)) {
                $where .= " AND ";
                $flag = true;
                $i = 0;
                foreach($event_category as $key=>$cat_id ) {
                    if($i != 0) $where .= ' OR ';
                    $where .= " (node.vid IN (  SELECT tn.vid FROM term_node tn  WHERE tn.tid  = $cat_id ))";
                    $i++;
                }
          }
    }
    if($event_location && is_array($event_location))  {
          if(!in_array(0,$event_location)) {
                if($flag) {
                  $where .= " OR ";
                } else {
                  $where .= " AND ";
                }
                $i = 0;
                foreach($event_location as $key=>$loc_id ) {
                    if($i != 0) $where .= ' OR ';
                    $where .= " (node.vid IN (  SELECT tn.vid FROM term_node tn  WHERE tn.tid  = $loc_id ))";
                    $i++;
                }
          }
    }

    if($startdate) {
        $event_time = date('Y-m-d',strtotime($startdate));
        $where .= " AND (node_data_field_event_date.field_event_date_value LIKE ('%$event_time%'))";
    }

    if($enddate) {
        $event_time = date('Y-m-d',strtotime($enddate));
        $where .= " AND (node_data_field_event_date_end.field_event_date_end_value = LIKE ('%$event_time%'))";
    }

    $sql = "SELECT COUNT( DISTINCT node.nid, node_data_field_event_date.field_event_date_value ) AS count FROM node node
            LEFT JOIN content_field_event_date node_data_field_event_date ON node.vid = node_data_field_event_date.vid
            LEFT JOIN content_field_event_date_end node_data_field_event_date_end ON node.vid = node_data_field_event_date_end.vid
            WHERE (node.type in ('event'))" . $where;

    $results = db_query($sql);
    if($results) {
      return db_fetch_object($results);
    } else {
      return false;
    }
}
function get_pueblo_events($startdate, $enddate, $event_category, $event_location, $events_per_page = 10) {
    if(isset($_GET['paged']) && $_GET['paged']>0){
        $current_page = (int)$_GET['paged'];
    }else{
        $current_page = 1;
    }

    $offset=$events_per_page*($current_page-1);
    $where = "";
    $flag = false;
    if($event_category && is_array($event_category))  {
          if(!in_array(0,$event_category)) {
                $where .= " AND ";
                $flag = true;
                $i = 0;
                foreach($event_category as $key=>$cat_id ) {
                    if($i != 0) $where .= ' OR ';
                    $where .= " (node.vid IN (  SELECT tn.vid FROM term_node tn  WHERE tn.tid  = $cat_id ))";
                    $i++;
                }
          }
    }
    if($event_location && is_array($event_location))  {
          if(!in_array(0,$event_location)) {
                if($flag) {
                  $where .= " OR ";
                } else {
                  $where .= " AND ";
                }
                $i = 0;
                foreach($event_location as $key=>$loc_id ) {
                    if($i != 0) $where .= ' OR ';
                    $where .= " (node.vid IN (  SELECT tn.vid FROM term_node tn  WHERE tn.tid  = $loc_id ))";
                    $i++;
                }
          }
    }

    if($startdate) {
        $event_time = date('Y-m-d',strtotime($startdate));
        $where .= " AND (node_data_field_event_date.field_event_date_value LIKE ('%$event_time%'))";
    }

    if($enddate) {
        $event_time = date('Y-m-d',strtotime($enddate));
        $where .= " AND (node_data_field_event_date_end.field_event_date_end_value = LIKE ('%$event_time%'))";
    }

    $end = " ORDER BY node_data_field_event_date.field_event_date_value DESC";

    if($events_per_page) $end .= " LIMIT ".$events_per_page;

    if($offset) $end .= " OFFSET ".$offset;

    $sql = "SELECT node.nid AS nid, 
	node_data_field_event_thumb.field_event_thumb_fid AS node_data_field_event_thumb_field_event_thumb_fid, 
	node_data_field_event_thumb.field_event_thumb_list AS node_data_field_event_thumb_field_event_thumb_list, 
	node_data_field_event_thumb.field_event_thumb_data AS node_data_field_event_thumb_field_event_thumb_data, 
	node.type AS node_type, 
	node.vid AS node_vid, 
	node.title AS node_title, 
	node_revisions.body AS node_revisions_body, 
	node_revisions.format AS node_revisions_format, 
	node_data_field_event_date.field_event_date_value AS node_data_field_event_date_field_event_date_value, 
	node_data_field_event_date.field_event_date_value2 AS node_data_field_event_date_field_event_date_value2, 
	node_data_field_event_date.field_event_date_rrule AS node_data_field_event_date_field_event_date_rrule, 
	node_data_field_event_date.delta AS node_data_field_event_date_delta 
	
	FROM node node  	
	LEFT JOIN content_field_event_date node_data_field_event_date ON node.vid = node_data_field_event_date.vid 
	LEFT JOIN term_node term_node ON node.vid = term_node.vid 
	LEFT JOIN content_field_event_thumb node_data_field_event_thumb ON node.vid = node_data_field_event_thumb.vid 
	LEFT JOIN node_revisions node_revisions ON node.vid = node_revisions.vid 
	WHERE ((node.status <> 0) 
	AND (node.type in ('event')) " . $where . $end;
	
	/*"AND (term_node.tid IS NOT NULL) 
	AND (term_node.tid IS NOT NULL)) 
	AND ((DATE_FORMAT(ADDTIME(node_data_field_event_date.field_event_date_value, SEC_TO_TIME(-25200)), '%Y-%m-%d') >= '2011-02-13') 
	OR ((DATE_FORMAT(ADDTIME(node_data_field_event_date.field_event_date_value, SEC_TO_TIME(-25200)), '%Y-%m-%d') <= '2011-02-13' 
	AND DATE_FORMAT(ADDTIME(node_data_field_event_date.field_event_date_value2, SEC_TO_TIME(-25200)), '%Y-%m-%d') >= '2011-02-13')))
	ORDER BY node_data_field_event_date_field_event_date_value ASC

	
	
	SELECT DISTINCT node.nid AS nid,
            node.title AS node_title,
           node_data_field_event_date.field_event_date_value AS node_data_field_event_date_field_event_date_value,
           node_data_field_event_date.field_event_date_rrule AS node_data_field_event_date_field_event_date_rrule,
           node_data_field_event_date.delta AS node_data_field_event_date_delta,
           node.type AS node_type,
           node.vid AS node_vid


            FROM node node
            LEFT JOIN content_field_event_date node_data_field_event_date ON node.vid = node_data_field_event_date.vid
            LEFT JOIN content_field_event_date_end node_data_field_event_date_end ON node.vid = node_data_field_event_date_end.vid
            WHERE (node.type in ('event')) */

    $results = db_query($sql);
    $nodes = array();
    if($results) {
        while ($node_nid = db_fetch_object($results)) {
          $nodes[] = $node_nid;
        }
    } else {
      $nodes = false;
    }
    return $nodes;
}
function get_pueblo_lodging($lodging_area = '', $checkin = '', $checkout = '') {
    $where = "";

    if($lodging_area)  {
              $lid = (int) $lodging_area;
                    $where .= "  AND (node.vid IN ( SELECT tn.vid FROM term_node tn WHERE tn.tid  = $lid ))";
    }

    if($checkin) {
        $lodging_time = strtotime($checkin);
        $where .= " AND (node.created >= $lodging_time )";
    }

    if($checkout) {
        $lodging_time = strtotime($checkout);
        $where .= " AND (node.created <= $lodging_time )";
    }

    $sql = "SELECT node.nid AS nid FROM node node  WHERE (node.type in ('lodging'))" . $where;

    $results = db_query($sql);
    $nodes = array();
    if($results) {
        while ($node_nid = db_fetch_object($results)) {
          $nodes[] = node_load($node_nid->nid);
        }
    } else {
      $nodes = false;
    }
    return $nodes;
}
function get_paginate_links($total_items, $entries_per_page)
{
    if(isset($_GET['paged']) && $_GET['paged']>0){
        $current_page = $_GET['paged'];
    }else{
        $current_page = 1;
    }
    $curr_uri = check_plain(request_uri());

    if($curr_uri[0]=='/'){
        $limit = 999;
        if(strpos($curr_uri, '?')!==false){
            $limit = strpos($curr_uri, '?')-1;
        }
        $curr_uri = substr($curr_uri, 1, $limit);
    }

	$total_page = ceil($total_items / $entries_per_page);
    $showing = '';
    if( $total_items < $entries_per_page )
    {
      $showing = "<div class=\"pager\"><em class=\"caption\">Showing <span>$total_items</span> Events</em></div>";
    }
	if($total_items > $entries_per_page)
    {
        $interval = '';
        if($current_page == 1) {
          $interval = '1';
        } else {
          $pfirst = $current_page * $entries_per_page;
          $interval .= $pfirst;
          $psecond = $pfirst + $entries_per_page;
          if( $psecond != $pfirst && ($psecond <= $total_items) ) $interval .= '-'.$psecond;
        }
		$paginate_links = '<div class="pager">';
            $paginate_links .= "<em class=\"caption\">Showing <span>$interval</span> of <span>$total_items</span> Events</em>";
			$paginate_links .= '<ul>';

			if ($current_page == 1) {
				$paginate_links .= "";
    		} else {
                unset($_GET['q']);
                $_GET['paged'] = $current_page - 1;
                $url = url($curr_uri, array('query'=>$_GET));
				$paginate_links .= '<li><a class="btn-prev" href="'.$url.'">prev</a></li>';
			}

			for ($i = 1; $i <= $total_page; $i++) {
                unset($_GET['q']);
                $_GET['paged'] = $i;
                $url = url($curr_uri, array('query'=>$_GET));
				if ($i == $current_page) {
					$paginate_links .= '<li class="active"><a class="active num'.$i.'" href="'.$url.'">'.$i.'</a></li>';
				} else {
					$paginate_links .= '<li><a href="'.$url.'">'.$i.'</a></li>';
				}
			}

			if ($current_page >= $total_page) {
				$paginate_links .= "";
			} else {
                unset($_GET['q']);
                $_GET['paged'] = $current_page + 1;
                $url = url($curr_uri, array('query'=>$_GET));
				$paginate_links .= '<li><a class="btn-next" href="'.$url.'">next</a></li>';
			}
			$paginate_links .= '</ul>';
		$paginate_links .= '</div>';
		return $paginate_links;
	}
    if($showing) return $showing;
}
?>
