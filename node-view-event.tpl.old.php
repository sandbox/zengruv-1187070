<h4><a href="<?php print $node_url ?>"><?php print $node->title; ?></a></h4>
<?php
    $event_teaser = current($node->body);
    $event_lodging = current($node->field_event_lodging);
    $event_amenties = current($node->field_event_amenties);
    $event_date = current($node->field_event_date);
    if(isset( $_GET['event_delta'] )) {
        $event_delta = (int)$_GET['event_delta'];
        $event_date = $node->field_event_date[$event_delta];
        $event_date_end = $node->field_event_date_end[$event_delta];
    } else {
        $event_date = current($node->field_event_date);
        $event_date_end = current($node->field_event_date_end);
    }
    $estart = $event_date['value'];
    $eend = $event_date_end['value'];
    $estart = strtotime($estart);
    $eend = strtotime($eend);
    if($estart && $estart != $eend && isset($eend)) $eventtime = date('ga',$estart). ' - '.date('ga',$eend);
    if(isset($estart) && !isset($eend)) $eventtime = date('ga',$estart);
    $estart = date('F jS',$estart);
    $terms = taxonomy_node_get_terms($node);
    foreach( $terms as $eterm ) {
        if( $eterm->vid == 3 ) $elocation = $eterm->name;
    }
?>
<ul class="dates">
        <?php if($estart): ?><li><a href="#"><?php print $estart; ; ?></a></li><?php endif; ?>
        <?php if($eventtime): ?><li><?php print $eventtime; ?></li><?php endif; ?>
</ul>
<?php if($elocation): ?><em class="location"><?php print $elocation; ?></em><?php endif; ?>
<div class="hold">
<?php
    if(is_array($node->field_event_thumb)):
     $image = current($node->field_event_thumb);
     $image_path = $image['filepath'];
?>
        <div class="photo photo-right">
                <div class="bg1">
                        <div class="bg2">
                                <div class="bg3">
                                    <?php
                                        print theme('imagecache', 'event_image', $image_path, $title) ;
                                    ?>
                                </div>
                        </div>
                </div>
        </div>
<?php
    endif;
    //if($event_teaser):
?>
        <p><?php print $event_teaser['value'] ?></p>
<?php
    //endif;
?>
</div>
<ul class="tools">
        <li><a href="<?php print $node_url ?>">More Info</a></li>
        <?php if($event_lodging): ?><li><a href="<?php print $event_lodging['value'] ?>">Lodging</a></li><?php endif; ?>
        <?php if($event_amenties): ?><li><a href="<?php print $event_amenties['value'] ?>">Amenties</a></li><?php endif; ?>
</ul>
