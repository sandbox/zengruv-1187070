<?php if ( arg(0) == 'node' && is_numeric(arg(1)) && ! arg(2) ) {
  $node = node_load(arg(1));
}
?>
<?php if (!empty($block->subject)): ?>
	<div class="heading<?php
	 if($node->nid == 3): ?> green-heading<?php endif; 
	 if($node->type == 'activity'): ?> green-heading<?php endif; 
	 if($node->type == 'event'): ?> orange-heading<?php endif; 
	 if($node->type == 'community'): ?> red-heading<?php endif; 
	 if($node->type == 'organization'): ?> red-heading<?php endif; 
	 if($node->type == 'elected_official'): ?> red-heading<?php endif; 
	 if($node->nid == 91): ?> blue-heading<?php endif; 
	 if($node->nid == 90): ?> magenta-heading<?php endif; 
	 ?>">
		<h2><?php print $block->subject ?></h2>
	</div>
<?php endif;?>
<?php print $block->content ?>
