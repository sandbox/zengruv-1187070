<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
  <head>
    <?php print $head ?>
    <title><?php print $head_title ?></title>
    <?php print $styles ?>
    <link type="text/css" rel="stylesheet" href="<?php echo base_path() . path_to_theme(); ?>/css/jquery-ui-1.8.4.custom.css" media="screen"/>
    <?php print $scripts ?>
    <!--[if lt IE 7]>
      <?php print phptemplate_get_ie7_styles(); ?>
    <![endif]-->
    <!--[if lt IE 8]>
      <?php print phptemplate_get_ie8_styles(); ?>
    <![endif]-->
  </head>
  <body<?php print phptemplate_body_class($left, $right); ?>>
	<div class="wrapper">
		<!-- logo -->
		<strong class="logo"><a href="http://pueblo.org">Pueblo, Colorado.</a></strong>
		<img src="<?php echo base_path() . path_to_theme(); ?>/images/logo-print.gif" alt="PuebloColorado" class="hidden" />
		<!-- main -->
		<div id="main" class="home">
			<div class="main-bg">
				<!-- threecolumns -->
				<div class="threecolumn">
					<div class="columns">
						<!-- center column -->
						<div class="col-center">
							<!-- text box -->
							<div class="text-box">
							    <?php if ($mission): print '<div id="mission">'. $mission .'</div>'; endif; ?>
							    <?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block">'; endif; ?>
							    <?php if ($tabs): print '<ul class="tabs primary">'. $tabs .'</ul></div>'; endif; ?>
							    <?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
							    <?php if ($show_messages && $messages): print $messages; endif; ?>
							    <?php print $help; ?>
							    <?php print $content ?>
							</div>
							<!-- search box 
							<div class="search-box">
								<h2>Visit Us - Find Lodging in Pueblo</h2>
								<!-- pattern box --
								<div class="pattern-box">
									<div class="pattern-t">
										<div class="pattern-b">
										  <?php
											  $lodging_list_url = url('node/82');
										  ?>
											<form action="<?php print $lodging_list_url; ?>" method="get">
												<fieldset>
													<input type="hidden" name="act" value="searchlodging" />
													<legend class="hidden">Find Lodging in Pueblo</legend>
													<?php
														$all_terms = custom_get_terms_by_vocabulary(4);

													?>
													<div class="row">
														<select name="lodgingarea" title="select area of town">
															<option>Select Area of Town</option>
															<?php
																if(is_array($all_terms)):
																  foreach($all_terms as $key=>$area_term):
															?>
															<option value="<?php print $area_term->tid; ?>"><?php print $area_term->name; ?></option>
															<?php
																  endforeach;
																endif;
															?>
														</select>
													</div>
													<div class="row">
														<div class="col1 calendar-col">
															<label for="start-date">Check In:</label>
															<input type="text" name="checkin" class="txt" id="start-date" />
														</div>
														<div class="col1 calendar-col">
															<label for="end-date">Check Out:</label>
															<input type="text" name="checkout" class="txt" id="end-date" />
														</div>
														<div class="col2">
															<input type="submit" class="btn-search" value="Search &raquo;" title="Search" />
														</div>
													</div>
												</fieldset>
											</form>
										</div>
									</div>
								</div>
								<a href="#" class="lnk-view">View Hotel Directory</a>
							</div>
							post box -->
							<?php print $front_content; ?>
						</div>
						<!-- left sidebar -->
					    <?php if ($left): ?>
					      <div class="sidebar green-bar">
						<?php print $left ?>
					      </div>
					    <?php endif; ?>
					</div>
					<!-- right sidebar -->
					<div class="aside orange-side">
						<?php print $right; ?>
					</div>
				</div>
			</div>
		</div>
		<!-- header -->
		<div id="header" class="home">
			<div class="bg">
				<!-- gallery -->
				<?php print $header; ?>
			</div>
		</div>
		<!-- headline -->
		<div id="headline">
			<!-- search form -->
			<?php if ($search_box): ?><div class="search"><?php print $search_box ?></div><?php endif; ?>
			<ul class="top-menu">
				<!-- <li><a href="#"><span>My pueblo</span></a></li> -->
				<li><a href="/jobs">Jobs</a></li>
				<li><a href="/search">Search:</a></li>
			</ul>
		</div>
	</div>
	<!-- footer -->
	<div id="footer">
		<div class="wrapper">
			<?php print $footer; ?>
		</div>
	</div>
  <?php print $closure ?>
  </body>
</html>
