<h3><?php print $node->title; ?></h3>
<?php
    if(is_array($node->field_lodging_thumb)):
     $image = current($node->field_lodging_thumb);
     $image_path = $image['filepath'];
?>
<div class="img-col">
        <div class="photo">
                <div class="bg1">
                        <div class="bg2">
                                <div class="bg3">
                                        <a href="<?php print $node_url ?>">
                                                <?php
                                                    print theme('imagecache', 'event_image', $image_path, $title) ;
                                                ?>
                                        </a>
                                </div>
                        </div>
                </div>
        </div>
</div>
<?php endif; ?>
<div class="descr-col">
        <?php if($node->field_lodging_teaser): ?>
        <?php
            $lodging_teaser = current($node->field_lodging_teaser);
        ?>
        <p><?php print($lodging_teaser['value']) ?></p>
        <?php
            endif;
        ?>
        <a href="<?php print $node_url ?>" class="lnk-view">Book Now!</a>
</div>