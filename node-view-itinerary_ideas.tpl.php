<?php
    if(is_array($node->field_thumb)):
     $image = current($node->field_thumb);
     $image_path = $image['filepath'];
?>
<div class="itinerary-block">
<div class="photo">
    <div class="bg1">
        <div class="bg2">
            <div class="bg3">
                <a href="<?php $node_url; ?>"><?php print theme('imagecache', '2col_right_story', $image_path, $title) ; ?></a>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>
<h3 style="clear: left;"><?php print $title; ?></h3>
<?php $event_teaser = current($node->field_event_teaser);
    if($event_teaser):
?>
<p><?php print $event_teaser['value']; ?></p>
<?php endif; ?>
</div>
<ul class="link-list link-list-green">
    <li><a href="http://pueblo.org/visit/activities/family<?php //print $node_url; ?>">Learn More</a></li>
    <li><a href="http://pueblo.org/visit/pueblo-itinerary<?php //print url('node/83'); ?>">See More Itinerary Ideas</a></li>
</ul>
