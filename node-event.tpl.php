<?php
    $event_teaser = current($node->field_event_teaser);
    $event_lodging = current($node->field_event_lodging);
    $event_amenties = current($node->field_event_amenties);
    $event_contact = current($node->field_event_contact);
    $event_contact_phone = current($node->field_event_contact_phone);
	$field_email = current($node->field_email);
    $field_event_website = current($node->field_website);
    $event_calendar_url = current($node->field_evnt_calendar_url);
    $event_date_array = current($node->field_event_date);
    $event_date = explode(" | ", $event_date_array['view']);
    $edate = $event_date[0];
    $etime = $event_date[1];
	$location = current($node->locations);
	$elocation = $location[name];
	$eaddress_string = $location[street].' '.$location[additional].' '.$location[city].', '.$location[province].' '.$location[postal_code];
	$event_map_url = 'http://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q='.$location[name].'+'.$location[street].'+'.$location[city].'+'.$location[province].'+'.$location[postal_code].'+'.$location[country].'&aq=0&t=h&z=14';
?>
<div class="headline">
    <!--
    <ul class="social">
        <li><a class="facebook" href="#">facebook</a></li>
        <li><a class="twitter" href="#">twitter</a></li>
        <li><a class="mail" href="#">mail</a></li>
        <li><a class="rss" href="#">rss</a></li>
    </ul>
    -->
    <h2 class="main-title-text"><?php print $title; ?></h2>
    <dl>
        <dt>Categories:</dt>
        <?php
            $terms = taxonomy_node_get_terms($node);
            if($terms):
            foreach( $terms as $eterm ) {
                if( $eterm->vid == 3 ) $ecommunity = $eterm->name;
            }
        ?>
        <dd>
          <?php
              $flag = 0;
              foreach( $terms as $eterm ) {
                  if( $eterm->vid == 2 ) {
                      if($flag != 0) print ', ';
                      print $eterm->name;
                      $flag++;
                  }
              }
          ?>
        </dd>
        <?php
            else:
        ?>
        <dd> - </dd>
        <?php
            endif;
        ?>
    </dl>
</div>
<div class="post">
<?php
    if(is_array($node->field_event_thumb)):
        $image = current($node->field_event_thumb);
        $image_path = $image['filepath'];
?>
    <div class="photo">
        <div class="bg1">
            <div class="bg2">
                <div class="bg3">
                    <a href="<?php print $node->nid; ?>"><?php print theme('imagecache', 'single_node_image', $image_path, $title) ; ?></a>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<pre><?php //print_r($location); ?></pre>
    <div class="caption">
        <ul class="date">
        	<li><span><?php print $edate; ?></span></li>
            <li><?php print $etime; ?></li>
        </ul>
        <em class="location"><?php if($elocation) print $elocation; if($event_map_url): ?><br />
        <span><?php print $eaddress_string?></span> <a href="<?php print $event_map_url ?>">(map)</a>
		<?php endif; ?></em>
        <?php print $content; ?>
        <?php if($event_contact['value'] ): ?>
        <dl class="contcat">
            <dt>Contact:</dt>
            <dd><?php if($field_email['view'] ): print '<a href="mailto:'.$field_email['safe'].'">'; endif;
			print $event_contact['value']; 
			if($field_email['view'] ): print '</a>'; endif;
			if($event_contact_phone['value'] ): print ' - '.$event_contact_phone['value']; endif;
			?>
        </dl>
        <?php endif; ?>
        <ul class="sub-nav">
            <?php if($field_event_website): ?><li><strong>More Information: </strong><a href="<?php print $field_event_website['display_url'] ?>"><?php print $field_event_website['display_title'] ?></a></li><?php endif; ?>
            <?php if($event_calendar_url): ?><li><a href="<?php print $event_calendar_url['value']; ?>">Add Event to Your Calendar</a></li><?php endif; ?>
            <?php if($event_lodging): ?><li><a href="<?php print $event_lodging['value'] ?>">Lodging</a></li><?php endif; ?>
            <?php if($event_amenties): ?><li><a href="<?php print $event_amenties['value'] ?>">Ameneties</a></li><?php endif; ?>
        </ul>
    </div>
</div>
    <?php print $single_event_bottom; ?>
<?php if($single_event_bottom): ?>
<div class="tabs-box events-tabs">
  <div class="tabs-nav">
      <ul>
          <li class="active"><a href="#"><span><em>Most Viewed Events</em></span></a></li>
          <li><a href="#"><span><em>Most Shared Events</em></span></a></li>
          <li><a href="#"><span><em>Events on the Same Date</em></span></a></li>
      </ul>
  </div>
  <div class="tabs-content">
    <?php print $single_event_bottom; ?>
  </div>
</div>
<?php endif; ?>