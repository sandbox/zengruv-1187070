<?php
    if(is_array($node->field_thumb)):
     $image = current($node->field_thumb);
     $image_path = $image['filepath'];
?>
<div class="sidebar-photo">
	<span class="photo-t">&nbsp;</span>
	<div class="photo-m">
                    <a href="<?php print $node_url; ?>"><?php print theme('imagecache', '2col_right_story', $image_path, $title) ; ?></a>
	</div>
	<span class="photo-b">&nbsp;</span>
</div>
<?php
    endif;
?>
    <h3><?php print $title; ?></h3>
<?php
    $field_teaser = current($node->field_teaser);
    $field_urbanrenewal_link = current($node->field_urbanrenewal_link);
?>
    <?php if($field_teaser): ?><p><?php print $field_teaser['value']; ?></p><?php endif; ?>
    <ul class="link-list link-list-red">
	    <li><a href="<?php print $node_url; ?>">Learn&nbsp;More</a></li>
	    <?php  if($field_urbanrenewal_link):?><li><a href="<?php print $field_urbanrenewal_link['url'] ?>"><?php print $field_urbanrenewal_link['title'] ?></a></li><?php endif; ?>
    </ul>