<div class="weather">
    <span class="top">&nbsp;</span>
    <div class="middle">
        <div class="middle-hold">
            <img src="<?php print $weather['image']['filename']; ?>" <?php print $weather['image']['size']; ?> alt="<?php print $weather['condition']; ?>" title="<?php print $weather['condition']; ?>" class="img-weather" />
            <ul class="weather-tools">
                <li class="active"><a target="_blank" href="http://www.weather.com/weather/today/Pueblo+CO+USCO0323">Currently</a></li>
                <li><a target="_blank" href="http://www.weather.com/weather/hourbyhour/graph/USCO0323">Hour-by-Hour</a></li>
                <li><a target="_blank" href="http://www.weather.com/weather/tenday/USCO0323">10 day</a></li>
            </ul>
            <strong class="deg">
                <?php if (isset($weather['temperature'])): ?>
                    <?php print t("!temperature",
                      array('!temperature' => $weather['temperature'])); ?>
                <?php endif ?>
            </strong>
            <span class="sunny"><?php print $weather['condition']; ?></span>
            <a target="_blank" href="http://www.weather.com/weather/today/Pueblo+CO+USCO0323" class="button-more">More&nbsp;</a>
        </div>
    </div>
    <span class="bottom">&nbsp;</span>
</div>