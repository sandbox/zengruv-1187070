					<div class="center-col">
						<div class="text-wrapper">
							<div class="holder">
								<?php if ($title): print '<h2 class="'. ($tabs ? ' with-tabs' : '') .' main-title-text">'. $title .'</h2>'; endif; ?>
								<?php print $content ?>
							</div>
						</div>
					</div>

