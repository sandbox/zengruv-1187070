<em class="date">THIS WEEK</em>
<h3><a href="<?php print $node_url ?>"><?php print $node->title; ?></a></h3>
<div class="hold">
<?php
    $event_teaser = current($node->field_event_teaser);
    if(is_array($node->field_event_thumb)):
     $image = current($node->field_event_thumb);
     $image_path = $image['filepath'];
?>
    <div class="photo-col">
        <div class="photo">
            <div class="bg1">
                <div class="bg2">
                    <div class="bg3">
                        <a href="<?php print $node_url ?>"><?php print theme('imagecache', 'event_image', $image_path, $title) ; ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
    endif;
    if($event_teaser):
?>
    <div class="descr-col">
        <p><?php print $event_teaser['value'] ?></p>
    </div>
<?php
    endif;
?>
</div>
<p><a href="<?php print $node_url; ?>" class="lnk-view lnk-view-orange">Event Details</a></p>