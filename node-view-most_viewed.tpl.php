<?php
    $event_teaser = current($node->field_event_teaser);
    $event_lodging = current($node->field_event_lodging);
    $event_amenties = current($node->field_event_amenties);
    $event_date = current($node->field_event_date);
    $event_date_end = current($node->field_event_date_end);
    $estart = $event_date['value'];
    $eend = $event_date_end['value'];
    $estart = strtotime($estart);
    $eend = strtotime($eend);
    if($estart && $estart != $eend) $eventtime = date('ga',$estart). ' - '.date('ga',$eend);
    if($estart && $estart == $eend) $eventtime = date('ga',$estart);
    $estart = date('F j, Y',$estart);
    $terms = taxonomy_node_get_terms($node);
    foreach( $terms as $eterm ) {
        if( $eterm->vid == 3 ) $elocation = $eterm->name;
    }
?>
    <ul class="date">
        <li><a href="#"><?php print $estart; ?> </a></li>
        <?php if($eventtime): ?><li><?php print $eventtime; ?></li><?php else: ?><li>All Day</li><?php endif; ?>
    </ul>
    <?php
        if(is_array($node->field_event_thumb)):
         $image = current($node->field_event_thumb);
         $image_path = $image['filepath'];
    ?>
    <div class="photo-col">
        <div class="photo">
            <div class="bg1">
                <div class="bg2">
                    <div class="bg3">
                        <a href="<?php print $node_url; ?>"><?php print theme('imagecache', 'today_event_thumb', $image_path, $title) ; ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
        endif;
    ?>
    <div class="text">
        <strong class="title"><a href="<?php print $node_url; ?>"><?php print $title; ?></a></strong>
        <?php if($event_teaser): ?><p><?php print $event_teaser['value']; ?></p><?php endif; ?>
        <ul class="sub-nav">
            <li><a href="<?php print $node_url; ?>">More Info</a></li>
            <?php if($event_lodging): ?><li><a href="<?php print $event_lodging['value']; ?>">Lodging</a></li><?php endif; ?>
            <?php if($event_amenties): ?><li><a href="<?php print $event_amenties['value'] ?>">Amenties</a></li><?php endif; ?>
        </ul>
    </div>
