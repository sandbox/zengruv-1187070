// accordion function
function initAccordion() {
	var _activeClass = 'active';
	var _slideSpeed = 500;

	jQuery('ul.subnav-list').each(function(){
		var _accordion = jQuery(this);
		var _items = _accordion.find('li:has(.slide)');
		_items.each(function(){
			var _holder = jQuery(this);
			var _opener = _holder.find('>a');
			var _slider = _holder.find('>.slide');

			_opener.click(function(){
				var _levelItems = _holder.parent().children(':has(.slide)').not(_holder);

				if(_holder.hasClass(_activeClass)) {
					_slider.slideUp(_slideSpeed,function(){
						_holder.removeClass(_activeClass);
					});
				} else {
					_holder.addClass(_activeClass);
					_slider.slideDown(_slideSpeed);

					// collapse others
					_levelItems.find('>.slide:visible').slideUp(_slideSpeed,function(){
						_levelItems.removeClass(_activeClass);
					})
				}
				return false;
			});

			if(_holder.hasClass(_activeClass)) _slider.show();
			else _slider.hide();
		});
	});
}