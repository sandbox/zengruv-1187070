/* cufon config */
function initCufon() {
	Cufon.replace('div.navigation > ul > li > a', { textShadow: '#333 1px 1px', fontFamily: 'AugereauBold', hover: true });
}
/* clearInputs */
function clearInputs(){
	jQuery('input[type=text], input[type=password]').each(function(){
		this.val = this.value;
		this.onfocus = function() {
			if(this.value == 'Password') { this.value = '';}
		}
		/*this.onblur = function() {
			if(this.value == '') { this.value = this.val;}
		}*/
	});
}
/* IE6 hover */
function ieHover(h_list){
	if(jQuery.browser.msie && jQuery.browser.version < 7){
		jQuery(h_list).hover(function(){
			jQuery(this).addClass('hover');
		}, function(){
			jQuery(this).removeClass('hover');
		});
	}
}
/* tabs */
function initTabs(){
	jQuery('div.tabs-box').each(function(){
		var _hold = jQuery(this);
		var _btn = _hold.find('div.tabs-nav li');
		var _box = _hold.find('div.tabs-content div.tab');
		var _a = _btn.index(_btn.filter('.active:eq(0)'));
		if(_a == -1) _a = 0;
		_btn.removeClass('active').eq(_a).addClass('active');
		_box.removeClass('active').css({
			position: 'absolute',
			top:'-9999px',
			left:'-9999px'
		});
		_box.eq(_a).addClass('active').css({
			position: 'relative',
			top:'0',
			left:'0'
		});

		_btn.click(function(){
			changeTab(_btn.index(this));
			return false;
		});

		function changeTab(_ind){
			if(_ind != _a){
				_btn.eq(_a).removeClass('active');
				_btn.eq(_ind).addClass('active');
				_box.eq(_a).removeClass('active').css({
					position: 'absolute',
					top:'-9999px',
					left:'-9999px'
				});
				_box.eq(_ind).addClass('active').css({
					position: 'relative',
					top:'0',
					left:'0'
				});
				_a = _ind;
			}
		}
	});
}

function eventDatepick(){
	$('#edit-field-event-date-value-wrapper').each(function(){
		var _inp = $(this).children(':lt(4)').not(':eq(0)');
		_inp.css({
			position: 'absolute',
			left: '-9999px',
			top: '-9999px'
		});
		$(this).children(':eq(0)').after('<input id="start-date" type="text" class="form-text" style="float:left;" />');
		var allDates = jQuery('#start-date, #end-date');
		var dates = jQuery('#start-date').datepicker({
			showOn: 'button',
			buttonImage: '/sites/all/themes/pueblocolorado/images/ico-calendar.gif',
			buttonImageOnly: true,
			showOn: 'both',
			defaultDate: "0",
			onSelect: function(selectedDate) {
				var option = this.id == "start-date" ? "minDate" : "maxDate";
				var instance = jQuery(this).data("datepicker");
				var date = jQuery.datepicker.parseDate(instance.settings.dateFormat || jQuery.datepicker._defaults.dateFormat, selectedDate, instance.settings);
				allDates.not(this).datepicker("option", option, date);
				_inp.eq(1).find('select option').removeAttr('selected').filter('[value='+parseInt(selectedDate.split('/')[0],10)+']').attr('selected','selected');
				_inp.eq(2).find('select option').removeAttr('selected').filter('[value='+parseInt(selectedDate.split('/')[1],10)+']').attr('selected','selected');
				_inp.eq(0).find('select option').removeAttr('selected').filter('[value='+selectedDate.split('/')[2]+']').attr('selected','selected');
			}
		});
		jQuery('#start-date').val(_inp.eq(1).find('select option:selected').val()+'/'+
				_inp.eq(2).find('select option:selected').val()+'/'+
				_inp.eq(0).find('select option:selected').val());

	});
	$('#edit-field-event-date-end-value-wrapper').each(function(){
		var _inp = $(this).children(':lt(4)').not(':eq(0)');
		_inp.css({
			position: 'absolute',
			left: '-9999px',
			top: '-9999px'
		});
		$(this).children(':eq(0)').after('<input id="end-date" type="text" class="form-text" style="float:left;" />');
		var allDates = jQuery('#start-date, #end-date');
		var dates = jQuery('#end-date').datepicker({
			showOn: 'button',
			buttonImage: '/sites/all/themes/pueblocolorado/images/ico-calendar.gif',
			buttonImageOnly: true,
			showOn: 'both',
			defaultDate: "0",
			onSelect: function(selectedDate) {
				var option = this.id == "start-date" ? "minDate" : "maxDate";
				var instance = jQuery(this).data("datepicker");
				var date = jQuery.datepicker.parseDate(instance.settings.dateFormat || jQuery.datepicker._defaults.dateFormat, selectedDate, instance.settings);
				allDates.not(this).datepicker("option", option, date);
				_inp.eq(1).find('select option').removeAttr('selected').filter('[value='+parseInt(selectedDate.split('/')[0],10)+']').attr('selected','selected');
				_inp.eq(2).find('select option').removeAttr('selected').filter('[value='+parseInt(selectedDate.split('/')[1],10)+']').attr('selected','selected');
				_inp.eq(0).find('select option').removeAttr('selected').filter('[value='+selectedDate.split('/')[2]+']').attr('selected','selected');
			}
		});
		jQuery('#end-date').val(_inp.eq(1).find('select option:selected').val()+'/'+
				_inp.eq(2).find('select option:selected').val()+'/'+
				_inp.eq(0).find('select option:selected').val());
	});
	$('#edit-field-event-date-end-rrule-UNTIL-datetime-wrapper').each(function(){
		$(this).parents('.fieldset-wrapper:eq(0)').css('display','block');
		var _inp = $(this).children(':lt(4)').not(':eq(0)');
		_inp.css({
			position: 'absolute',
			left: '-9999px',
			top: '-9999px'
		});
		$(this).children(':eq(0)').after('<input id="until-date" type="text" class="form-text" style="float:left;" />');
		var allDates = jQuery('#start-date, #until-date');
		var dates = jQuery('#until-date').datepicker({
			showOn: 'button',
			buttonImage: '/sites/all/themes/pueblocolorado/images/ico-calendar.gif',
			buttonImageOnly: true,
			showOn: 'both',
			defaultDate: "0",
			onSelect: function(selectedDate) {
				var option = this.id == "start-date" ? "minDate" : "maxDate";
				var instance = jQuery(this).data("datepicker");
				var date = jQuery.datepicker.parseDate(instance.settings.dateFormat || jQuery.datepicker._defaults.dateFormat, selectedDate, instance.settings);
				allDates.not(this).datepicker("option", option, date);
				_inp.eq(1).find('select option').removeAttr('selected').filter('[value='+parseInt(selectedDate.split('/')[0],10)+']').attr('selected','selected');
				_inp.eq(2).find('select option').removeAttr('selected').filter('[value='+parseInt(selectedDate.split('/')[1],10)+']').attr('selected','selected');
				_inp.eq(0).find('select option').removeAttr('selected').filter('[value='+selectedDate.split('/')[2]+']').attr('selected','selected');
			}
		});
		jQuery('#until-date').val(_inp.eq(1).find('select option:selected').val()+'/'+
				_inp.eq(2).find('select option:selected').val()+'/'+
				_inp.eq(0).find('select option:selected').val());
	});
}

jQuery('document').ready(function(){
	clearInputs();
	initCufon();
	initTabs();
	ieHover('.navigation li, .tabs-nav li');
	jQuery('ul.accordion').accordion({
		active: ".selected",
		autoHeight: false,
		header: ".acc-head",
		collapsible: true,
		event: "click"
	});
	initAccordion();
	//var dates = jQuery('#start-date, #end-date').datepicker({
	//	showOn: 'button',
	//	buttonImage: '/sites/all/themes/pueblocolorado/images/ico-calendar.gif',
	//	buttonImageOnly: true,
	//	defaultDate: "0",
	//	onSelect: function(selectedDate) {
	//		var option = this.id == "start-date" ? "minDate" : "maxDate";
	//		var instance = jQuery(this).data("datepicker");
	//		var date = jQuery.datepicker.parseDate(instance.settings.dateFormat || jQuery.datepicker._defaults.dateFormat, selectedDate, instance.settings);
	//		dates.not(this).datepicker("option", option, date);
	//	}
	//});
	//eventDatepick();
});