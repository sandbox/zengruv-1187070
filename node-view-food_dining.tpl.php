<div class="text">
<h3><?php print $title ?></h3>
<?php
    if(is_array($node->field_dining_thumb)):
     $image = current($node->field_dining_thumb);
     $image_path = $image['filepath'];
?>
      <div class="photo photo-right">
          <div class="bg1">
              <div class="bg2">
                  <div class="bg3">
<?php
                    print theme('imagecache', 'pueblo_dining', $image_path, $title) ;
?>
                  </div>
              </div>
          </div>
      </div>
<?php
    endif;
    print $content;
?>
    <ul class="tools">
        <li><a href="<?php print $node_url; ?>">More Info</a></li>
        <?php if($node->field_lodging || $node->field_tickets || $node->field_photos ): ?>
            <?php $field_lodging = current($node->field_lodging); ?>
            <?php $field_tickets = current($node->field_tickets); ?>
            <?php $field_photos = current($node->field_photos);  ?>
            <?php if($field_lodging): ?>
            <li><a href="<?php echo $field_lodging['value'] ?>">Lodging</a></li>
            <?php endif; ?>
            <?php if($field_tickets): ?>
            <li><a href="<?php echo $field_tickets['value'] ?>">Tickets</a></li>
            <?php endif; ?>
            <?php if($field_photos): ?>
            <li><a href="<?php echo $field_photos['value'] ?>">Photos</a></li>
            <?php endif; ?>
        <?php endif; ?>
    </ul>
</div>