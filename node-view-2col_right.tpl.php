<?php
    if(is_array($node->field_thumb)):
     $image = current($node->field_thumb);
     $image_path = $image['filepath'];
?>
    <div class="photo">
            <div class="bg1">
                    <div class="bg2">
                            <div class="bg3">
                                    <a href="<?php print $node_url; ?>">
                                        <?php print theme('imagecache', '2col_right_story', $image_path, $title) ; ?>
                                    </a>
                            </div>
                    </div>
            </div>
    </div>
<?php
    endif;
?>
    <h3><?php print $title; ?></h3>
    <?php
        if($node->field_story_teaser):
        $field_story_teaser = current($node->field_story_teaser);
        if($field_story_teaser):
    ?>
    <p><?php print $field_story_teaser['value']; ?></p>
    <?php
        endif;
    ?>
    <ul class="link-list link-list-green">
            <li><a href="<?php print $node_url; ?>">Learn More</a></li>
    </ul>
    <?php
        endif;
    ?>