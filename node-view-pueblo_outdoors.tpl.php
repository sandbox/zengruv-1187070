<?php
    if(is_array($node->field_thumbnail)):
     $image = current($node->field_thumbnail);
     $image_path = $image['filepath'];
?>
      <div class="photo">
          <div class="bg1">
              <div class="bg2">
                  <div class="bg3">
<?php
                    print theme('imagecache', 'pueblo_outdoors', $image_path, $title) ;
?>
                  </div>
              </div>
          </div>
      </div>
<?php
    endif;
?>
<div class="text">
<?php if ($page == 0): ?>
  <h3><?php print $title ?></h3>
<?php endif; ?>
<?php print $content ?>
<?php if($node->field_trail_maps_url || $node->field_web_cams ): ?>
  <ul class="tools">
      <?php $field_trail_maps_url = current($node->field_trail_maps_url); ?>
      <?php $field_web_cams = current($node->field_web_cams); ?>
      <?php if($field_trail_maps_url): ?>
        <li><a href="<?php echo $field_trail_maps_url['value'] ?>">Trail Maps</a></li>
      <?php endif; ?>
      <?php if($field_web_cams): ?>
        <li><a href="<?php echo $field_web_cams['value'] ?>">Web Cams</a></li>
      <?php endif; ?>
  </ul>
<?php endif; ?>
</div>